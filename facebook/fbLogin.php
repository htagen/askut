<?php
session_start();

// Kaasame SDK poolt kaasa pandud autoloaderi.
require_once __DIR__ . '/facebook-php-sdk/src/Facebook/autoload.php';

// Kaasame tarvilikud teegid.
use Facebook\Facebook;

//Üritame luua uut FB objekti.
try {
    $fb = new Facebook([
        'app_id' => '186213238771598',
        'app_secret' => '35bad9c320272e0691852eef1bacd723',
        'default_graph_version' => 'v2.2',
    ]);
} catch (\Facebook\Exceptions\FacebookSDKException $e) {
    echo "Error!";
}
$helper = $fb->getRedirectLoginHelper();

$permissions = ['email']; // Valikuline luba
//Suuname fbCallback.php'sse, et võimaldada/ alustada autentimisega.
$loginUrl = $helper->getLoginUrl('https://askut.today/facebook/fbCallback.php', $permissions);

header("Location: $loginUrl");