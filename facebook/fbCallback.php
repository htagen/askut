<?php
session_start();

require_once __DIR__ . '/facebook-php-sdk/src/Facebook/autoload.php';

//Proovime luua uut Facebook objekti infoga, mille FB developer lehel sain/ seadistasin.
try {
    $fb = new Facebook\Facebook([
        'app_id' => '186213238771598',
        'app_secret' => '35bad9c320272e0691852eef1bacd723',
        'default_graph_version' => 'v2.2',
    ]);
} catch (\Facebook\Exceptions\FacebookSDKException $e) {
    echo $e->getMessage();
}

$helper = $fb->getRedirectLoginHelper();

try {
    $accessToken = $helper->getAccessToken('https://askut.today/facebook/fbCallback.php');
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    // Graph tagastab errori
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    // Valideerimine ebaõnnestub või muud koodisisesed vead
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

if (! isset($accessToken)) {
    if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
    }
    exit;
}

//Kasutaja on sisselogitud
echo '<h3>Access Token</h3>';
var_dump($accessToken->getValue());

// OAuth 2.0 client handler aitab juurdepääsu token'eid hallata
$oAuth2Client = $fb->getOAuth2Client();

// Hangime juurdepääsu token'i metadata /debug_token'ist
$tokenMetadata = $oAuth2Client->debugToken($accessToken);
echo '<h3>Metadata</h3>';
var_dump($tokenMetadata);

// Valideerimine.
try {
    $tokenMetadata->validateAppId('186213238771598');
} catch (\Facebook\Exceptions\FacebookSDKException $e) {
}
try {
    $tokenMetadata->validateExpiration();
} catch (\Facebook\Exceptions\FacebookSDKException $e) {
}

if (! $accessToken->isLongLived()) {
    // Vahetab lühiajalised (short-lived) access token'id pikaajaliste (long-lived) token'ite vastu.
    try {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
        exit;
    }
    echo '<h3>Long-lived</h3>';
    var_dump($accessToken->getValue());
}

$_SESSION['fb_access_token'] = (string) $accessToken;

try {
    // Tagastab `Facebook\FacebookResponse` objekti
    $response = $fb->get('/me?fields=id,name,email', $accessToken);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

try {
    $fbUserInfo = $response->getGraphUser();
    $_SESSION['fbUserId'] = $fbUserInfo['id'];
    $_SESSION['fbUserName'] = $fbUserInfo['name'];
    $_SESSION['fbUserEmail'] = $fbUserInfo['email'];

} catch (\Facebook\Exceptions\FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

// Kasutaja on sisselogitud pikaajalise access token'iga.
// Suuname ta meldinutele kätte saadavasse AskUT'sse.
header('Location: https://askut.today/home/main.php');