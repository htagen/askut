<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>KKK | AskUT</title>
    <meta name="description" content="Statistika lehekülastuste kohta." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, stats, statistics, statistika, kasutajad, browser, ip, os, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
?>
<?php include 'header.php' ?>
<div class="main" itemscope itemtype="http://schema.org/WebPage">
    <div class="whiteBoxContainer" id="faqBox" itemscope itemtype="http://schema.org/QAPage">
        <div class="questionsAndAnswersContainer" itemprop="mainContentOfPage">
            <h3 itemprop="headline" id="kkk">Külastajate statistika</h3><br>
            <div><?php include '../database/getUserStats.php'?></div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>
</body>
</html>