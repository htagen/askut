<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>Minu vastused | AskUT</title>
    <meta name="description" content="Minu küsitud küsimused AskUT portaalis." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, minu küsimused, minu küsitud küsimused, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
include_once '../database/server.php';
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
//Kontrollimaks, kas kasutaja on sisse logitud
if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "Enda vastuste vaatamiseks pead sa sisse logima";
    header("location: http://askut.today");
} ?>
<?php include 'header.php' ?>
<div class="main" itemscope itemtype="http://schema.org/WebPage">
    <div class="questionsContainer">
        <div class="col-1"></div>
        <div class="col-8 whiteBoxContainer" id="questionsBox">
            <h2 itemprop="headline" id="questionsHeader">MINU VASTUSED</h2>
            <hr id="pageLine">

            <div class="tableContainer" itemprop="mainContentOfPage">
                <?php include "../database/myanswerssql.php"; ?>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
<?php include 'footer.php' ?>
</body>
</html>
