<!DOCTYPE html>
<html lang="en" xml:lang="en" class="registerBody">
<head>
    <title>Logi sisse | AskUT</title>
    <meta name="description" content="Logi sisse AskUT portaali." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, logi sisse, login, sisene, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <?php include 'head.php' ?>
</head>
<body class="registerBody" itemscope itemtype="http://schema.org/WebPage">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
?>
<?php include ('../database/server.php') ?>
<div id="registerHeader">
    <a id="registerAskUTlogo" href="http://askut.today">
        <p itemprop="headline">AskUT</p>
    </a>
</div>
<main class="main" id="loginPageMain" itemscope itemtype="http://schema.org/WebPage">
    <div class="whiteBoxContainer" id="loginContainer">
        <div class="fieldAreaContainer" id="loginFieldContainer">
            <form class="fieldForm" method="post" action="login.php?link=login" id="loginForm" autocomplete="off">
                <?php include "../database/errors.php"?>
                <div>
                <?php
                if (isset($_SESSION['message'])){
                    echo $_SESSION['message'];
                }
                ?>
                </div>
                <br>
                <div class="fieldGroup">
                    <input name="username" title="Kasutajanimi" class="fieldFormInput" itemprop="target" id="loginUsernameField" type="text" required oninvalid="this.setCustomValidity('Sisesta kasutajanimi!')" oninput="setCustomValidity('')">
                    <span class="fieldHighlight"></span>
                    <span class="bar"></span>
                    <label itemprop="name" class="fieldFormLabel" id="loginUsernameLabel">Kasutajanimi*</label>
                </div>
                <div class="fieldGroup">
                    <input name="password" title="Parool" class="fieldFormInput" itemprop="target" id="loginPasswordField" type="password" required oninvalid="this.setCustomValidity('Sisesta parool!')" oninput="setCustomValidity('')">
                    <span class="fieldHighlight"></span>
                    <span class="bar"></span>
                    <label itemprop="name" class="fieldFormLabel" id="loginPasswordLabel">Parool*</label>
                </div>
                <div>
                   <button class="loginButton loginPageLogin" type="submit" name="loginUser">Logi sisse</button>
                    <button class="loginButton loginPageLogin" onclick="location.href='https://askut.today'" type="button">Katkesta</button>
                </div>
                <p>Pole veel kasutajat? Registreeri end <a class="registerHereLink" href="register.php">siin!</a></p>
<!--                <p>Unustasid parooli ja/või kasutajanime? Taasta oma andmed <a class="registerHereLink" href="restorePasswd.php">siin!</a></p>-->
            </form>
        </div>
    </div>
</main>
</body>
</html>