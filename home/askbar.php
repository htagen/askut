    <!-- askbar -->

    <!--<div id="askBar" class="whiteBoxContainer">
        <div class="askBarContent">
            <span id="closeAsk">&times;</span>
            <div id="askBody">
                <form method=post action="" class="askForm">
                    <div class="fieldAskName" itemscope itemtype="http://schema.org/SearchAction">
                        <label>Aine nimi:* </label>
                        <input required title="Sisesta aine nimi" name="subject"
                               class="fieldFormInput" itemprop="target" autocomplete="off"
                               type="text" oninvalid="this.setCustomValidity('Palun sisesta aine nimi!')"
                               oninput="setCustomValidity('')" />
                        <div class="resultName"></div>
                    </div>
                    <div class="fieldAskCode" itemscope itemtype="http://schema.org/SearchAction">
                        <label>Ainekood:* </label>
                        <input required name="code" class="fieldFormInput" id="askCode" type="text"
                               oninvalid="this.setCustomValidity('Palun sisesta ainekood!')"
                               oninput="setCustomValidity('')"/>
                    </div>
                    <label>Õppejõud: </label>
                    <input class="fieldFormInput" id="askProf" name="lector"/>
                    <div>
                        <button class="loginButton" type="submit" name="ask">Küsi!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
-->
        <div id="askBar" class="whiteBoxContainer">
            <div class="askBarContent">
                <span id="closeAsk">&times;</span>
                <div id="askBody" class="askBar">
                    <div class="fieldAskName">
                        <label id="askLabel">Aine nimi:* </label>
                        <br>
                        <input required name="subject" title="Sisesta aine nimi"
                               class="fieldFormInput askInput" itemprop="target" autocomplete="off"
                               type="text" oninvalid="this.setCustomValidity('Palun sisesta aine nimi!')"
                               oninput="setCustomValidity('')" />
                        <div class="resultName"></div>
                    </div>
                    <div class="fieldAskCode">
                        <label id="askLabel">Ainekood:* </label>
                        <br>
                        <input required name="code" class="fieldFormInput askInput" id="askCode" type="text"
                               oninvalid="this.setCustomValidity('Palun sisesta ainekood!')"
                               oninput="setCustomValidity('')"/>
                    </div>
                    <label id="askLabel">Õppejõud: </label>
                    <br>
                    <input class="fieldFormInput askInput" id="askProf" name="lector"/>
                    <div>
                        <button class="loginButton" id="askBtn" type="submit" name="ask">Küsi!</button>
                    </div>
                </div>
            </div>
        </div>
