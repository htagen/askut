<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>AskUT | AskUT</title>
    <meta name="description" content="Mis on AskUT? Miks ja kellele on seda saiti vaja ning kes on portaali autorid." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, mis on AskUT, autor, autorid, AskUT eesmärk, toetavad, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <?php include '../home/head.php' ?>
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script src="../dataPush/client/client2.js"></script>
</head>
<body>
<h1>Response from server:</h1>
<div id="response"></div>
</body>
</html>
