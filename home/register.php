<!DOCTYPE html>
<html lang="en" xml:lang="en" class="registerBody">
<head>
    <title>Registreeri | AskUT</title>
    <meta name="description" content="Registreeru AskUT portaali." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, registreeri, sign up, registreeru, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <?php include 'head.php' ?>
    <?php include 'languages.php' ?>
</head>
<body class="registerBody" itemscope itemtype="http://schema.org/WebPage">
<?php
session_start();
?>

<div id="registerHeader">
    <a id="registerAskUTlogo" href="http://askut.today">
        <p itemprop="headline">AskUT</p>
    </a>
</div>
<div class="registerAreaContainer">
    <div id="registerArea">

        <?php include ('../database/server.php') ?>
        <?php include ('../database/errors.php');?>
        <h1 id="registerHeading">Registreeru</h1>
        <p id="registerAbout">Palun täida järgnevad väljad, et luua endale konto.</p>


        <form method="post" action="register.php" >
            <label class="registerLabel" >Kasutajanimi*</label>
            <input class="fieldFormInput registerInputField" id="user" type="text" name="username" title="registerUsername" required oninvalid="this.setCustomValidity('Sisesta kasutajanimi!')" oninput="setCustomValidity('')">
            <br>

            <label class="registerLabel">Email*</label>
            <input class="fieldFormInput registerInputField" id="email" type="email" name="email" title="registerEmail" required oninvalid="this.setCustomValidity('Sisesta email!')" oninput="setCustomValidity('')">
            <br>

            <label class="registerLabel">Eriala</label>
            <input class="fieldFormInput registerInputField" type="text" title="registerCourse" name="course">
            <br>

            <label class="registerLabel">Instituut</label>
            <input class="fieldFormInput registerInputField" type="text" title="registerInstitute" name="institute">
            <br>

            <label class="registerLabel" >Parool*</label>
            <input class="fieldFormInput registerInputField" id="pass1" type="password" name="password" title="registerPassword" required oninvalid="this.setCustomValidity('Sisesta parool!')" oninput="setCustomValidity('')">
            <br>

            <label class="registerLabel">Korda parooli*</label>
            <input class="fieldFormInput registerInputField" id="pass2" type="password" name="repeatPassword" title="registerRPassword" required oninvalid="this.setCustomValidity('Sisesta parool!')" oninput="setCustomValidity('')">

            <div id="registerButtons">
                <button class="loginButton loginPageLogin" type="submit" id="registerUser" name="register">Registreeru</button>
                <button class="loginButton loginPageLogin" onclick="window.location.href='https://askut.today/'" type="button">Katkesta</button>
            </div>
        </form>
    </div>
</div>
</body>

