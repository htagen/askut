<html lang="en" xml:lang="en" class="landingBody">
<?php include 'headLanding.php' ?>
<?php include 'languages.php' ?>
<?php include '../database/userStats.php'?>
<?php
if  (isset($_GET['anonymous'])) {
    header("location: http://askut.today/home/main.php?anonymous=1");
}
?>

<body class="landingBody">
<div id="landingContainer" itemscope itemtype="http://schema.org/WebPage">
    <div id="landingHeader">
        <a href="http://askut.today" id="landingLogo">
            <p itemprop="headline" id="headline">AskUT</p>
        </a>
    </div>
    <div id="loginAndSlogan" >
        <div id="landingSlogan">
            <p id="sloganID" itemprop="about"><?php echo languageFun('slogan');?></p>
        </div>
        <div id="loginButtonsAndLanguage" itemprop="mainContentOfPage">

            <a id="loginBtn" itemprop="url" class="loginButton" href="home/login.php"><span class="iconProperties icon1"></span> <?php echo languageFun('login');?></a>
            <a id="loginFbButton" itemprop="url" class="loginButton" href="facebook/fbLogin.php"><span class="iconProperties icon2"></span> <?php echo languageFun('loginFb');?> </a>
            <?php include_once 'loginphp/idLogin.php'?>
            <a id="loginIDButton" itemprop="url" class="loginButton" href="?idLogin=true"><span class="iconProperties icon3"></span> <?php echo languageFun('loginId');?></a>
            <a id="registerButton" itemprop="url" class="loginButton" href="home/register.php"><span class="iconProperties icon4"></span> <?php echo languageFun('register');?></a>
            <div id="anonymousEnterContainer">
                <a itemprop="url" id="anonymousEnter" href="?anonymous='1'"><?php echo languageFun('anonymous');?></a>
                <div class="userHelpPopup" onclick="userHelpInfo()">
                    <img itemprop="image" src="images/helpingInfo.svg" alt="?">
                    <span class="popuptext" id="userInfoPopup"><?php echo languageFun('anonInfo');?></span>
                </div>
            </div> 
            <div id="languageIcons">
                <a itemprop="url" id="est" href="?lang=est" class="languageSelect"<?php $lang = 'est'?>><span class="languageIconProperties est" aria-label="[Eesti keel]"></span></a>
                <a itemprop="url" id="eng" href="?lang=en" class="languageSelect"<?php $lang = 'en'?>><span class="languageIconProperties eng" aria-label="[English]"></span></a>
            </div>
        </div>
    </div>
    <div id="planeWrap" itemscope itemtype="http://schema.org/Thing">
        <img itemprop="image" id="plane" src="images/landing_background_plane.svg" alt="AskUT taustapilt - paberlennuk"/>
    </div>
</div>
</body>
</html>