<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>Õppeained | AskUT</title>
    <meta name="description" content="Andmebaas AskUT portaalis esinevatest Tartu Ülikooli õppeainetest." />
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
?>
<?php include 'header.php' ?>
<div class="main" itemscope itemtype="http://schema.org/WebPage">
    <div class="questionsContainer">
        <div class="col-1"></div>
        <div class="col-8 whiteBoxContainer" id="questionsBox">
            <hr id="pageLine">
            <div class="tableContainer" itemprop="mainContentOfPage">
                <?php include "../database/subjectssql.php"; ?>
            </div>
        </div>
        <div class="col-1"></div>
    </div>
</div>
<?php include 'footer.php' ?>
</body>
</html>
