<header id="headerContainer" itemscope itemtype="http://schema.org/WPHeader">
    <?php
    if (isset($_GET['anonymous']) && htmlspecialchars($_GET["anonymous"]) == '1' ){
        session_destroy();
        session_unset();
        unset($_SESSION['username']);
        unset($_SESSION['fbUserId']);
        unset($_SESSION['idUserId']);
        //$_SESSION = array();
    }
    if  (isset($_GET['logout']) && htmlspecialchars($_GET['logout']) == 'true') {
        session_destroy();
        session_unset();
        unset($_SESSION['username']);
        unset($_SESSION['fbUserId']);
        unset($_SESSION['idUserId']);
        header("location: https://askut.today");

    }
    if  (isset($_GET['link'])) {
        header("location: https://askut.today");
    }
    ?>
    <div class="headerNav">
        <div id="headerMenu">

            <div class="burgerButton jsToggleMenu" title="Menüü">
                <div class="burger"></div>
                <div class="burger"></div>
                <div class="burger"></div>
            </div>

            <!-- vasakpoolne menüü -->
            <a itemprop="url" class="headerMenuLink" id="sub" href="subjects.php">ÕPPEAINED</a>
            <a itemprop="url" class="headerMenuLink" id="ques" href="questions.php">KÜSIMUSED</a>

            <!-- logo-->
            <div class="headerMenuLink" id="logo">
                <a itemprop="url" href="main.php"><img itemprop="image" id="askUTlogo" src="../images/AskUTlogo2.svg" alt="AskUT logo"></a>
            </div>

            <!-- parempoolne menüü -->
            <a itemprop="url" class="headerMenuLink" id="faq" href="faq.php">KKK</a>
            <a itemprop="url" class="headerMenuLink" id="about" href="about.php">AskUT</a>
            <a itemprop="url" class="headerMenuLink" id="contact" href="contact.php">KONTAKT</a>

            <!-- kasutajaikoon/ kasutajaseadete nupp -->
            <div class="userMenuButton jsToggleMenuUser">
                <img itemprop="image" id="userIcon" src="../images/headerUserNoAvatar.svg" title="Konto seaded" alt="AskUT konto seaded">
            </div>
        </div>

        <!-- burger menüü -->
        <div class="stackedMenu jsCollapse">
            <ul class="stackedMenuList">
                <li class="mobileMenu" ><a itemprop="url" class="burgerMenuItem" href="subjects.php">• ÕPPEAINED</a></li>
                <li class="mobileMenu" ><a itemprop="url" class="burgerMenuItem" href="questions.php">• KÜSIMUSED</a></li>
                <li class="mobileMenu" ><a itemprop="url" class="burgerMenuItem" href="faq.php">• KKK</a></li>
                <li class="mobileMenu" ><a itemprop="url" class="burgerMenuItem" href="about.php">• AskUT</a></li>
                <li class="mobileMenu" ><a itemprop="url" class="burgerMenuItem" href="contact.php">• KONTAKT</a></li>
            </ul>
        </div>

        <!-- kasutaja menüü -->
        <div class="userMenuContainer jsCollapseUser">
            <ul class="userMenuList">
                <?php
                    if (isset($_SESSION['username']) || isset($_SESSION['fbUserId']) || isset($_SESSION['idUserId']))  : ?>
                        <li class="userMenu" ><a id="profileBtn" class="userMenuItem" href="profile.php">• Profiil</a></li>
                        <li class="userMenu" ><a id="myquestionsBtn" class="userMenuItem" href="myquestions.php">• Minu küsimused</a></li>
                        <li class="userMenu" ><a id="myanswersBtn" class="userMenuItem" href="myAnswers.php">• Minu vastused</a></li>
                        <li class="userMenu" ><a id="logOutBtn" class="userMenuItem" href="?logout=true">• Logi välja</a></li>
                    <?php else: ?>
                        <li class="userMenu" id="loginBtnDropDown" ><a class="userMenuItem" href="https://askut.today" >• Logi sisse</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</header>
