<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>KKK | AskUT</title>
    <meta name="description" content="Korduma kippuvad küsimused ja nende vastused." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, faq, kkk, korduma kipppuvad küsimused, frequently asked questions, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
?>
<?php include 'header.php' ?>
<div class="main" itemscope itemtype="http://schema.org/WebPage">
    <div class="whiteBoxContainer" id="faqBox" itemscope itemtype="http://schema.org/QAPage">
        <div class="questionsAndAnswersContainer" itemprop="mainContentOfPage">
            <h3 itemprop="headline" id="kkk">Korduma Kippuvad Küsimused</h3><br>
<!--            <a href="stats.php"><u>Statistika AskUT külastuste kohta</u></a>-->
            <h4 itemprop="text" id="faqQ1">Kus asub Tartu Ülikooli Arvutiteaduse instituudi õppehoone?</h4>
            <p itemprop="about" id="a1">• Aadress on Juhan Liivi 2, Tartu. All oleval kaardil on instituut ka näidatud.</p>
            <h4 itemprop="text" id="faqQ2">Kuidas ma saan enda profiilipilti muuta?</h4>
            <p itemprop="about" id="a2">• Paremal üleval nurgas on ümmargune profiili ikoon. Klikka sellel ning vali 'profiil' -> 'Muuda avatari'.</p>
            <h4 itemprop="text" id="faqQ3">Miks ma pean endale anonüümse saidi kasutamise jaoks konto looma?</h4>
            <p itemprop="about" id="a3">• Ei pea. Saidile saab sisse logida nii anonüümselt kui ka registreeritud kontosse sisse logitult.</p>
            <h4 itemprop="text" id="faqQ4">Mis on registreeritud konto eelised anonüümselt sisenemisele?</h4>
            <p itemprop="about" id="a4">• Anonüümselt sisenenud kasutaja saab saidil kõike näha (peale enda profiili osa) ja otsida, kuid ei saa esitada küsimusi ega vastata neile.
                Registreeritud kasutaja saab nii esitada küsimusi kui ka vastata teiste omadele, redigeerida enda profiili ning näeb enda esitatud küsimusi mugavalt enda profiililt.</p>
            <br><br>

            <div class="mapContainer" id="map" itemscope itemtype="http://schema.org/Place">

                <!-- <iframe itemprop="hasMap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2091.8985078772757!2d26.71248461627499!3d58.37824848133618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46eb36e1900bfbf1%3A0x5a26b1670e99e751!2sJuhan+Liivi+2%2C+50409+Tartu!5e0!3m2!1set!2see!4v1521112582538" width="800" height="300" style="border:0" allowfullscreen></iframe>
                -->
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>
<script src="../scripts/googleMapsMarkerAnimation.js" rel="script"></script>
<script  src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBRH7U3wim2Hin35UTbWffZwFz7MEVBBb0&callback=initMap&callback=initialize" rel="script"></script>
</body>
</html>
