<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>Profiil | AskUT</title>
    <meta name="description" content="Minu profiil AskUT portaalis." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, profiil, profiilipilt, minu profiil, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody" id="profile">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
include_once '../database/helper.php';
include_once '../database/server.php';

$_SESSION['url'] = $_SERVER['REQUEST_URI'];
//Kontrollimaks, kas kasutaja on sisse logitud
if (!isset($_SESSION['username']) && !isset($_SESSION['fbUserId'])) {
    $_SESSION['message'] = "Enda profiili vaatamiseks peab olema sisse logitud";
    header("location: https://askut.today");
}
?>
<?php include 'header.php' ?>
<div class="main" itemscope itemtype="http://schema.org/WebPage">
    <div class="whiteBoxContainer" id="profileBox">
        <h3 id="profileHeader" itemprop="headline">PROFIIL</h3>
        <?php include "../database/errors.php";?>
        <hr class="profileHR">
        <div class="profileInfoContainer">
            <div class="leftProfileArea">
                <img itemprop="image" class="avatarPicture" src="<?php echo $_SESSION['userAvatar']?>" alt="../images/user/doggo2.jpg">

                <form action="../database/avatarUpload.php" method="POST" enctype="multipart/form-data">
                    <p>
                        <label for="chooseFile">Vali fail: </label>
                        <input type="file" name="fileToUpload" id="chooseFile">
                        <label for="changeAvatarButton"></label>
                        <input class="loginButton" type="submit" value="Muuda avatari" name="submit" id="changeAvatarButton">
                    </p>
                </form>
            </div>
            <div class="profileInnerContainer">
                <div class="rightProfileArea" itemprop="mainContentOfPage">
                    <form action="profile.php" method="post" id="userInfoForm">
                        <table>
                            <tr class="profileGroup">
                                <td>
                                    <label class="profileLabel" for="username">Kasutajanimi*:</label>
                                </td>
                                <td>
                                    <input class="profileInput fieldFormInput fieldFormInput" id="username" name="username"  value="<?php echo $_SESSION['username']?>" required>
                                </td>
                            </tr>
                            <tr class="profileGroup">
                                <td>
                                    <label class="profileLabel" for="course" >Eriala:</label>
                                </td>
                                <td>
                                    <input class="profileInput fieldFormInput" id="course" name="course"  value="<?php if (isset($_SESSION['course'])) {
                                        echo $_SESSION['course'];
                                    }?>">
                                </td>
                            </tr>
                            <tr class="profileGroup">
                                <td>
                                    <label class="profileLabel" for="institute" >Instituut:</label>
                                </td>
                                <td>
                                    <input class="profileInput fieldFormInput" id="institute" name="institute"  value="<?php if (isset($_SESSION['institute'])){
                                        echo $_SESSION['institute'];
                                    }?>">
                                </td>
                            </tr>
                            <tr class="profileGroup">
                                <td>
                                    <label class="profileLabel" for="email">Email:</label>
                                </td>
                                <td>
                                    <input class="profileInput fieldFormInput" type="email" id="email" name="email"  value="<?php if (isset($_SESSION['email'])){
                                        echo $_SESSION['email'];
                                    }?>">
                                </td>
                            </tr>
                            <tr class="profileGroup">
                                <td>
                                    <label class="profileLabel" for="existingPassword" >Praegune parool:</label>
                                </td>
                                <td>
                                    <input class="profileInput fieldFormInput" type="password" id="existingPassword" name="existingPassword">
                                </td>
                            </tr>
                            <tr class="profileGroup">
                                <td>
                                    <label class="profileLabel" for="newPassword" >Uus Parool:</label>
                                </td>
                                <td>
                                    <input class="profileInput fieldFormInput" type="password" id="newPassword" name="newPassword" >
                                </td>
                            </tr>
                            <tr class="profileGroup">
                                <td>
                                    <label class="profileLabel" for="confirmNewPassword" >Korda uut parooli:</label>
                                </td>
                                <td>
                                    <input class="profileInput fieldFormInput" type="password" id="confirmNewPassword" name="confirmNewPassword" >
                                </td>
                            </tr>

                        </table>
                        <div id="profileButtonDiv">
                            <button class="loginButton" name="save" type="submit" id="saveChangesButton">Salvesta muudatused</button>

                        </div>
                        <div id="profileDeleteButtonDiv">
                            <button class="loginButton" name="deleteUser" id="deleteUser">Kustuta kasutaja</button>
                        </div>

                    </form>

                </div>

            </div>

        </div>

        <hr class="profileHR">
    </div>

</div>
<?php include 'footer.php' ?>
</body>
</html>