<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>AskUT | AskUT</title>
    <meta name="description" content="Mis on AskUT? Miks ja kellele on seda saiti vaja ning kes on portaali autorid." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, mis on AskUT, autor, autorid, AskUT eesmärk, toetavad, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
?>
<?php include 'header.php' ?>
<div class="main" itemscope itemtype="http://schema.org/WebPage">
    <div class="whiteBoxContainer" id="aboutBox">
        <div class="infoContainer" itemprop="mainContentOfPage">
            <h2 itemprop="headline">AskUT</h2><br>
            <h3 itemprop="text" id="whatQuestion">Mis?</h3>
            <p itemprop="about" id="whatAnswer">AskUT on Tartu Ülikooli õppeainetele loodud küsimuste esitamise portaal.</p>
            <h3 itemprop="text" id="whyQuestion">Miks?</h3>
            <p itemprop="about"  id="whyAnswer">Kas sa oled kunagi olnud suures audikas, kus on pea 100 tudengit koos? Ja hirmsasti tahaks teada,
                mis värk nende Pythoni funktsioonidega on või kui palju see 2+2 nüüd oligi või kuhu, käivad, lauses, need, komad?
                Või äkki on sul vaja leida projektikaaslane? AskUT lahendab kõik need küsimused. Sa ei pea enam kunagi tundma
                piinlikust, kui sa pole mingist triviaalsest baasteadmisest aru saanud. AskUT's pole lolle küsimusi.<br>Saa targaks juba täna!
                <br><br>Aga see pole veel kõik! Kui ikka üldse küsida ei taha ja kontot ka registreerida ei soovi, siis oleme ka Sulle mõelnud.
                AskUT saidile saad sisse logida ka täielikult anonüümsena, kus saad niisama ühikavoodil popkorni nosides teiste küsimusi sirvida.</p>
            <h3 itemprop="text" id="whoQuestion">Kes?</h3>
            <p itemprop="about"  id="whoAnswer">Idee ja teostuse autorid:</p>
            <div class="authorContainer">
                <div class="authorGroup">
                    <div class="polaroid">
                        <img itemprop="image" class="authorImg" src="../images/hanna.png" alt="Hanna Tagen" title="Hanna Tagen">
                        <div class="captionContainer">
                            <p itemprop="author" class="authorName">Hanna Tagen</p>
                        </div>
                    </div>
                    <div class="polaroid">
                        <img itemprop="image" class="authorImg" src="../images/carola.jpg" alt="Carola Kesküla" title="Carola Kesküla">
                        <div class="captionContainer">
                            <p itemprop="author" class="authorName">Carola Kesküla</p>
                        </div>
                    </div>
                    <div class="polaroid">
                        <img itemprop="image" class="authorImg" src="../images/kerttu.jpg" alt="Kerttu Talts" title="Kerttu Talts">
                        <div class="captionContainer">
                            <p itemprop="author" class="authorName">Kerttu Talts</p>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
            <p itemprop="about">PS! AskUT portaal on loodud Tartu Ülikooli Veebirakenduste loomise õppeaine raames.<br>2018</p><br>
            <p itemprop="text" id="supportedBy">Projekti toetavad:</p>
            <a itemprop="sponsor" class="tip" href="https://www.ut.ee" target="_blank" title="Tartu Ülikool"><img id="utLogo" src="../images/utLogo.png" alt="Tartu Ülikooli logo"></a>
            <a itemprop="sponsor" class="tip" href="http://www.hitsa.ee/ikt-haridus/ita" target="_blank" title="IT Akadeemia"><img id="itaLogo" src="../images/ita.png" alt="IT Akadeemia logo"></a>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>
</body>
</html>