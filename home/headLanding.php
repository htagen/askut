<head>
    <title>Kuidas soovid siseneda? | AskUT</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description" content="Tartu Ülikooli õppeainetele loodud anonüümne küsimisportaal."/>
    <meta name="keywords" content="AskUT, askut.today, ask ut, askut Tartu ülikool, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis, subjects, ask, questions, help, õppeained, küsimused, anonüümne, anonüümselt, anonüümsed, küsi, abi, õppimine, tudeng, õpiabi, mis, kuidas, miks, kes"/>
    <meta name="author" content="Hanna Tagen, Kerttu Talts, Carola Kesküla"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta property="og:image" content="http://askut.today/images/preview_image_logo_plane3.png" /> <!-- og ei toeta svg -->

    <!--    skriptid-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js" rel="script"></script>
    <script src="../scripts/cookies.js" rel="script"></script>
    <script src="../scripts/userHelpInfo.js" rel="script"></script>

    <!-- css failid -->
    <link href="styles/stylesheet.css" rel="stylesheet" type="text/css"/>
    <link href="styles/landing.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />

    <!--    varia-->
    <link rel="icon" href="images/favicon2.png">
    <link rel="shortcut icon" href="images/favicon2.png" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
    <link href="../manifest.json" rel="manifest">
</head>
