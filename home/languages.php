<?php
function languageFun($value) {

    if (isset($_GET['lang']))
        $lang = $_GET["lang"];
    else
        $lang = 'est';

    $languageArray = array('est', 'en');
    $found = false;

    if (in_array($lang, $languageArray))
        $found = true;
    if (!$found)
        $lang = 'est';

    $_SESSION['url'] = $_SERVER['REQUEST_URI'];

    if ($_SESSION['url'] == "/home/register.php") {
        $xml = simplexml_load_file("../xml/languages.xml");
    } else {
        $xml = simplexml_load_file("xml/languages.xml");
    }

    return $xml->$value->$lang;
};
