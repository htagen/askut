<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>Küsimused | AskUT</title>
    <meta name="description" content="Andmebaas Tartu Ülikooli õppeainete kohta esitatud küsimustest." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, küsimused, kõik küsimused, õppeainete küsimused, õppeaine küsimused, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/modal.js" rel="script"></script>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../scripts/loadMoreQuestions.js"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
?>
<?php include 'header.php' ?>
<?php include ('../database/server.php') ?>
<div class="main" itemscope itemtype="http://schema.org/WebPage">
    <div class="questionsContainer">
        <div class="col-1"></div>
        <div class="col-8 whiteBoxContainer" id="questionsBox">
            <hr id="pageLine">
            <div class="tableContainer" itemprop="mainContentOfPage">
                <div id="questionsTable">
                    <?php include "../loadMore/initialQuestions.php" ?>
                </div>
<!--                <button id="moreQuestions" class="loginButton">Lae veel</button>-->
            </div>
        </div>
        <?php include '../database/modal.php'?>
        <div class="col-1"></div>
    </div>
</div>
<?php include 'footer.php' ?>
</body>
</html>
