<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>Ajax võte</title>
    <script type="text/javascript" src="VL_demo2.js"></script>
    <meta name="description" content="Võta meiega ühendust!" />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, mis on AskUT, autor, autorid, AskUT eesmärk, toetavad, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
?>
<?php include 'header.php' ?>
<p>
    <a href="ajaxXML.xml" title="XML" id="loadXML">Load XML</a>
    |
    <a href="ajaxJSON.json" title="JSON" id="loadJSON">Load JSON</a>
</p>
<div id="courseData"></div>
</body>
</html>
