<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head>
    <?php
    if  (isset($_GET['logout']) && htmlspecialchars($_GET['logout']) == 'true') {
    session_destroy();
    session_unset();
    unset($_SESSION['username']);
    unset($_SESSION['fbUserId']);
    unset($_SESSION['idUserId']);
    header("location: https://askut.today");

    }
    ?>
    <title>AskUT</title>
    <meta name="description" content="Tartu Ülikooli õppeainetele loodud anonüümne küsimisportaal." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal,
    viimati küsitud küsimused, küsimused, küsi, otsi, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/modal.js" rel="script"></script>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <script src="https://code.jquery.com/jquery.min.js" rel="script"></script>
    <script src="../scripts/search.js" rel="script"></script>
    <script src="../scripts/askBar.js" rel="script"></script>
    <?php include 'head.php'; ?>
</head>
<body>
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
?>
<?php
include_once '../loginphp/idLogin.php';
include 'header.php'
?>

<div class="main" id="mainPageMain" itemscope itemtype="http://schema.org/WebPage">
    <div class="fieldAreaContainer">
        <!--Klikkimisel avab modali.-->
        <form class="fieldForm" method="post" itemscope itemtype="http://schema.org/SearchAction">
            <div class="fieldGroup">
                <!--Search toimimine: Otsib tabelitest 'question' ja 'subject' sarnaseid väärtusi. Kuvab 5
                esimest dropdownis koos nupuga "Veel...", mis avab modali kõikide väärtustega.
                modali leitud väärtustega.-->
                <input required title="Otsi küsimusi" class="fieldFormInput" itemprop="target"
                       id="searchField" type="text" name="searchQuery" autocomplete="off" oninvalid="this.setCustomValidity('Palun täida see väli!')"
                       oninput="setCustomValidity('')">
                <div class="result"></div>
                <span class="fieldHighlight"></span>
                <span class="bar"></span>
                <label itemprop="name" class="fieldFormLabel" id="searchLabel">Otsi küsimusi...</label>
                <button id="mainSearchButton" type="submit" value="submit" onclick="return chk()"><img itemprop="image" title="Otsi..." id="searchIcon"
                                                                 src="https://askut.today/images/search.svg" alt="Otsi portaalist AskUT"></button>
            </div>
        </form>
        <?php if (isset($_SESSION['username']) || isset($_SESSION['idUserId']) || isset($_SESSION['fbUserId'])) :?>
        <p id="questionVal"></p>
        <form class="fieldForm" method="post" itemscope itemtype="http://schema.org/SearchAction">
            <div class="fieldGroup2">
                <!--Küsi toimimine: Otsib tabelitest 'question' sarnaseid väärtusi. Kui leiab sarnaseid, siis avab
                kasutajale modali. Võimaldab kas nendele küsimustele vajutada või valida "Küsi uus küsimus"
                "Küsi uus küsimus" valimise korral avaneb JS'iga alla lisapaneel, kus peab valima/sisestama ainekoodi
                (autocomplete võimalus), ainenime (autocomplete võimalus) ja õppejõu nime
                (pole kohustuslik; (autocomplete võimalus)). Viimaks on nupp küsi.
                Küsi vajutades kontrollitakse lisaks üle, et identset küsimust juba ei eksisteeriks.
                Kui eksisteerib, siis kuvatakse kasutajale sellest teade ja suunatakse ta olemasoleva küsimuse modalisse.
                Kui ei eksisteeri, siis avatakse just küsitud küsimuse modal ning kasutaja küsitud küsimus on nähtav
                tema küsitud küsimuste tabelist.-->
                <input required title="Esita küsimus" class="fieldFormInput" name="question" itemprop="target"
                       autocomplete="off" type="text" id="questionField"
                       oninvalid="this.setCustomValidity('Palun sisesta küsimus!')"
                       oninput="setCustomValidity('')" onkeyup="onKeyInput()">
                <span class="fieldHighlight"></span>
                <span class="bar"></span>
                <label itemprop="name" class="fieldFormLabel" id="questionLabel">KÜSI...</label>
            </div>
            <?php include 'askbar.php'?>
        </form>
            <?php include ('../database/server.php') ?>
            <?php include "../database/errors.php";?>
        <?php endif; ?>
    </div>
    <div class="whiteBoxContainer" id="mainBox">
        <h2 id="mainRecentlyAskedAreaHeader" itemprop="headline">HILJUTI KÜSITUD</h2>
        <div class="mainQuestionBoxes" itemprop="mainContentOfPage">
            <?php include '../database/questionBox.php' ?>
        </div>
        <?php include '../database/modal.php'?>
    </div>
</div>
<?php include 'footer.php' ?>
</body>
</html>