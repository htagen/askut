<!DOCTYPE html>
<html lang="en" xml:lang="en" class="registerBody">
<head>
    <title>Taasta parool | AskUT</title>
    <meta name="description" content="Taasta oma AskUT konto parool." />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, logi sisse, login, sisene, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <?php include 'head.php' ?>
</head>
<body class="registerBody" itemscope itemtype="http://schema.org/WebPage">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
?>
<?php include ('../database/server.php') ?>
<div id="registerHeader">
    <a id="registerAskUTlogo" href="http://askut.today">
        <p itemprop="headline">AskUT</p>
    </a>
</div>
<div class="main" id="loginPageMain" itemscope itemtype="http://schema.org/WebPage">
    <div class="whiteBoxContainer" id="loginContainer">
        <div class="fieldAreaContainer" id="loginFieldContainer">
            <form class="fieldForm" method="post" action="restorePasswd.php" id="loginForm" autocomplete="off">
                <?php include "../database/errors.php"?>
                <div>
                    <?php
                    if (isset($_SESSION['message'])){
                        echo $_SESSION['message'];
                    }
                    ?>
                    <?php //var_dump($_SESSION); ?>
                </div>
                <br>
                <div class="fieldGroup">
                    <input name="username" title="Kasutajanimi või email" class="fieldFormInput" itemprop="target" id="loginUsernameField" type="text" required oninvalid="this.setCustomValidity('Sisesta kasutajanimi või email!')" oninput="setCustomValidity('')">
                    <span class="fieldHighlight"></span>
                    <span class="bar"></span>
                    <label itemprop="name" class="fieldFormLabel" id="restoreUsernameLabel">Kasutajanimi või email*</label>
                </div>
                <div>
                    <button class="loginButton loginPageLogin" type="submit" name="restore">Taasta parool</button>
                    <button class="loginButton loginPageLogin" onclick="location.href='https://askut.today'" type="button">Katkesta</button>
                </div>
                <p>Pole veel kasutajat? Registreeri end <a id="registerHereLink" href="register.php">siin!</a></p>
            </form>
        </div>
    </div>
</div>
</body>
</html>