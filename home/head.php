
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="Hanna Tagen, Kerttu Talts, Carola Kesküla">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="http://askut.today/images/preview_image_logo_plane3.png" />  <!-- og ei toeta svg -->

    <!-- skriptid -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js" rel="script"></script>
    <script src="../scripts/cookies.js" rel="script"></script>
    <script src="../scripts/userHelpInfo.js" rel="script"></script>


    <!-- css failid -->
    <link href="../styles/stylesheet.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/fields.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/about.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/profile.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/main.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/faq.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/footer.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/header.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/landing.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/login.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/register.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/table.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/join.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/modal.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../styles/contact.css" rel="stylesheet" type="text/css" media="screen">
    <link href="../manifest.json" rel="manifest">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" media="screen"/>


        <!--    varia-->
    <link rel="icon" href="../images/favicon2.png">
    <link rel="shortcut icon" href="../images/favicon2.png" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Zeyada" />
