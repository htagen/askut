<!DOCTYPE html>
<html lang="en" xml:lang="en" class="whiteBody">
<head>
    <title>Kontakt | AskUT</title>
    <meta name="description" content="Võta meiega ühendust!" />
    <meta name="keywords" content="AskUt, askut.today, anonüümne, anonüümselt, tudengiportaal, küsimisportaal, mis on AskUT, autor, autorid, AskUT eesmärk, toetavad, Tartu Ülikool, TÜ, UT, University of Tartu, Universitas Tartuensis "/>
    <script src="../scripts/dropdownMenus.js" rel="script"></script>
    <script src="../scripts/headerOnScroll.js" rel="script"></script>
    <?php include 'head.php' ?>
</head>
<body class="whiteBody">
<!--session vaatab, mis url'il kasutaja parasjagu on ja jätab selle meelde-->
<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
?>
<?php include 'header.php' ?>
<div class="main" itemscope itemtype="http://schema.org/WebPage">
    <div class="whiteBoxContainer" id="contactbox">
        <div class="fieldAreaContainer">
            <form class="fieldForm" method="post" action="contact.php" itemscope itemtype="http://schema.org/SearchAction">
                <?php include ('../database/server.php') ?>
                <?php include "../database/errors.php"?>
                <div>
                    <?php
                    if (isset($_SESSION['message'])){
                        echo $_SESSION['message'];
                    }
                    ?>
                    <?php //var_dump($_SESSION); ?>
                </div>
                <br>
                <div class="fieldGroup">
                    <input required title="email" name="email" class="fieldFormInput contactField" itemprop="target" id="contactEmailField" type="email" oninvalid="this.setCustomValidity('Palun sisesta email!')" oninput="setCustomValidity('')">
                    <span class="fieldHighlight"></span>
                    <span class="bar"></span>
                    <label itemprop="name" class="fieldFormLabel contactLabel" id="contactEmailLabel">Email</label>
                </div>
                <div class="fieldGroup">
                    <input required title="Teema" name="subject" class="fieldFormInput contactField" itemprop="target" type="text" id="contactSubjectField" oninvalid="this.setCustomValidity('Palun sisesta teema!')" oninput="setCustomValidity('')">
                    <span class="fieldHighlight"></span>
                    <span class="bar"></span>
                    <label itemprop="name" class="fieldFormLabel contactLabel" id="contactSubjectLabel">Teema</label>
                </div>
                <div class="fieldGroup">
                    <textarea required title="Sisu" name="message" class="fieldFormInput contactField" itemprop="target" id="contactBodyField" oninvalid="this.setCustomValidity('Palun kirjuta emaili sisu!')" oninput="setCustomValidity('')" rows="1" cols="10" wrap="soft"></textarea>
                    <span class="fieldHighlight"></span>
                    <span class="bar"></span>
                    <label itemprop="name" class="fieldFormLabel contactLabel" id="contactBodyLabel">Sisu</label>
                </div>
                <div>
                    <button class="sendBtn loginButton loginPageLogin" type="submit" name="sendMail">Saada</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>
</body>
</html>