$(document).ready(function () {
    var questionCount = 10; // võrdne initalquestions.php failis oleva sql lause LIMIT numbriga
    $("#moreQuestions").click(function () {
        questionCount = questionCount + 10; //suurendame igal korral 2 võrra ja näeme kaks küsimust igal nupu vajutuse korral rohkem
        $("#questionsTable").load("https://askut.today/loadMore/questionssql.php", {
            commentNewCount: questionCount
        });
    });
});