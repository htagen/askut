//Kui kasutaja vajutab "?" peale, siis kuvame talle abiinfot
function userHelpInfo() {
    var popup = document.getElementById("userInfoPopup");
    popup.classList.toggle("show");
    setTimeout(function(){
        popup.classList.remove("show");
    }, 3000);
}