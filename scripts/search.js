$(document).ready(function() {

    var chosenQ = [];
    $('.fieldGroup input[type="text"]').on("keyup input", function () {
        //otsime iga sisestuse peale
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings(".result");
        //FIXME: kasutaja peab sisestama vähemalt 3 tähte
        if (inputVal.length < 1) {
            resultDropdown.empty();
        } if (inputVal.length > 2) {
            $.get("../database/searchBackend.php", {term: inputVal}).done(function (questions) {
                // Kuvame dropdowni tulemustega.
                resultDropdown.html('');
                if (questions.length > 0) {
                    questions = $.parseJSON(questions);
                    chosenQ = questions;
                    $.each(questions, function (i, question) {
                        resultDropdown.append($('<p></p>', {
                            text: question.question
                        }))
                    });
                } else {
                    resultDropdown.append('<a>Ei leidnud midagi.. Küsi see küsimus ise!</a>')
                }
                //dropdownist välja vajutades sulgeme dropdowni
                $(document).on("click", ".main", function () {
                    resultDropdown.empty();
                });
            });
        }
    });

    //Küsimusele vajutades täidab fieldi küsimusega ja avab modali
    //FIXME: kuvab otsitud küsimuste andmed aga muudab ka teistes modalites nende andmed ainult selleks samaks
    $(document).on("click", ".result p", function () {
        $(this).parents(".fieldGroup").find('input[type="text"]').val($(this).text());
        $(this).parent(".result").empty();
        var modal = document.getElementById('mainModal');
        $('#modalAnswer').empty();
        modal.style.display = "block";
        var questions = chosenQ;
        for (var i = 0; i < questions.length; i++) {
            if (questions[i].question === $(this).text()) {
                var selectedQ = questions[i];
                $(".modalHeader h3").html($(this).text());
                $('#modalUserAndTime').html(selectedQ.username + " | " + selectedQ.time);
                $('#modalCodeNameLector').html(selectedQ.code + " | " + selectedQ.name + " | " + selectedQ.lector);
                //määrame läbi PHP hetkese ID session variableks, et seda hiljem vastates kasutada saaks
                $.ajax({
                    url: "../database/setSession.php",
                    type: 'POST',
                    data: {modalID: selectedQ.ID}
                });
            }
        }
        $.get("../database/getAnswers.php", {term: selectedQ.ID}).done(function (answers) {
            if (answers.length > 0) {
                answers = $.parseJSON(answers);
                //TODO: appendi modalisse vastused
                for (let i = 0; i < answers.length; i++) {
                    $('#modalAnswer').append($('<p></p>', {
                        text: answers[i].username + ": " + answers[i].answer
                    }))
                }
            } else {
                $('#modalAnswer').append('<b>Vastuseid ei ole. Kas oskad ehk vastata?</b>')
            }
        });
    });
});
