//https://developers.google.com veebilehelt näidete põhjal loodud


var marker;

function initialize() {
    var mapExists = document.getElementById('map');
    console.log(mapExists);

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: {lat: 58.3782565, lng: 26.7151122}

        });

        marker = new google.maps.Marker({
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP,
            position: {lat: 58.3782565, lng: 26.7151122},
            title: 'ATI'

        });
    var content =

        '</div>'+
        '<p><b>ATI</b>' +
        '<br>Arvutiteaduse instituut<br>' +
        'Tartu Ülikooli õppehoone <br>' +
        'Juhan Liivi 2 </p>'+

        '</div>';
        marker.addListener('click', toggleBounce);

    var infoWindow = new google.maps.InfoWindow({
        content: content
    });
    marker.addListener('click', function () {
        infoWindow.open(map,marker);

    });
    }

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }

    }

