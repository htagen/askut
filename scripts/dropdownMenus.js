window.addEventListener('load', function () {

    /*Kasutajamenüü*/
    var loweredDropdown = false;
    var dropdown = document.getElementsByClassName("jsCollapseUser")[0];

    document.getElementsByClassName("jsToggleMenuUser")[0].onclick = function () {
        if (!loweredDropdown) {
            dropdown.style.display = "block";
            if (isMenuOpen) {
                collapse.style.display = "none";
                isMenuOpen = false;
            }
            loweredDropdown = true;
        } else if (loweredDropdown) {
            dropdown.style.display = "none";
            loweredDropdown = false;
        }
    };

    /*Burgermenüü*/
    var isMenuOpen = false;
    var collapse = document.getElementsByClassName("jsCollapse")[0];

    document.getElementsByClassName("jsToggleMenu")[0].onclick = function () {
        if (!isMenuOpen) {
            collapse.style.display = "block";
            if (loweredDropdown) {
                dropdown.style.display = "none";
                loweredDropdown = false;
            }
            isMenuOpen = true;
        } else if (isMenuOpen) {
            collapse.style.display = "none";
            isMenuOpen = false;
        }
    };
});
