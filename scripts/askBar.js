$(document).ready(function () {
    var askbar = document.getElementById('askBar');
    askbar.style.display="none";
    var close = document.getElementById('closeAsk');

    close.addEventListener("click", function () {
        askbar.style.display = "none";
    });

    $('.fieldAskName input[type="text"]').on("keyup input", function(){
        //otsime iga sisestuse peale
        var inputValName = $(this).val();
        var resultDropdownName = $(this).siblings(".resultName");
        //FIXME: kasutaja peab sisestama vähemalt 3 tähte - H: arvan, et pole mõtet
        if(inputValName.length >= 1){
            $.get("../database/askNameBackend.php", {term: inputValName}).done(function(subjects) {
                // Kuvame dropdowni tulemustega.
                resultDropdownName.html(subjects);
                //dropdownist välja vajutades sulgeme dropdowni
                $(document).on("click", ".main", function () {
                    resultDropdownName.empty();
                });
            });
        } else{
            resultDropdownName.empty();
        }
    });

    //Aine nimele vajutades täidab input välja selle nimega
    //Otsib andmebaasist valitud aine ainekoodi ja täidab ainekoodi input välja sellega
    //TODO: kuva modalis selle küsimuse andmed
    $(document).on("click", ".resultName p", function(){
        // keel=JQuery-CSS
        $(this).parents(".fieldAskName").find('input[type="text"]').val($(this).text());
        $(this).parent(".resultName").empty();
        var subjectName = $(this).text();
        console.log(subjectName);
        $.get("../database/askNameBackend.php", {namescode: subjectName}).done(function(code) {
            $(".fieldAskCode").find('input[type="text"]').val(code);
        });
    });
});

function onKeyInput() {
    var askbar = document.getElementById('askBar');
    askbar.style.display = "none";
    var key = window.event.keyCode;
    //kasutaja vajutab enter
    if (key === 13) {
        askbar.style.display = "block";
        return false;
    }
}
