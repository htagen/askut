window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "rgba(255,255,255,0.7)",
                "text": "#000000"
            },
            "button": {
                "background": "#ffbe58",
                "text": "#353434"
            }
        },
        "theme": "classic",
        "content": {
            "link": "Learn more",
            "href": "https://www.cookielaw.org/the-cookie-law/",
            "rel": "noopener",
            "description": "Learn more"
        }
})});