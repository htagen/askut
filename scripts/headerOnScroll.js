window.addEventListener('load', function () {
    window.onscroll = function() {
        var scroll = window.pageYOffset;
        var headernav = document.getElementsByClassName("headerNav")[0];
        if (scroll >= 10) {
            headernav.style.background = "white";
        }
        /* clientWidht: IE8, innerWidth: kõik muud brauserid*/
        else if (window.innerWidth > 720 || window.clientWidth > 720) {
            headernav.style.background = "transparent"
        }
    }
});