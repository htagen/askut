window.addEventListener('load', function (){
    // Get the modal
    var modal = document.getElementById('mainModal');

    // Get the button that opens the modal
    var buttons = document.getElementsByClassName("jsMainQuestionButton");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    var buttonID;

    // function passVal(id){
    //     console.log(id);
    //
    //     var data = {
    //         fn: "id",
    //         str: id
    //     };
    //     $.post("../database/modal.php", data);
    // }

    // käib kogu klassi elemendid läbi
    for (var i = 0; i < buttons.length; i++){
        // When the user clicks on the button, open the modal

        // TÖÖTAV VERSIOON, ei garanteeri muidugi, et kõige optimaalsem/ parem :) -C

        buttonID = buttons[i].id;
        buttons[i].onclick = function () {
            modal.style.display = "block";
            var modalID = parseInt(buttons[i].id);

            //määrame läbi PHP hetkese ID session variableks, et seda hiljem vastates kasutada saaks
            $.ajax({
                url: "../database/setSession.php",
                type: 'POST',
                data: {modalID: modalID}
            });

            //$.post("../database/setSession.php", {name: 'modalID'});
            //võtame andmebaasist selle ID-ga küsimuse informatsiooni
            //FIXME: hetkel suvalises JS-failis :) tahtsin lihtsalt tööle saada
            $.get("../database/askNameBackend.php", {modal: modalID}).done(function(modalSubject) {
                modalSubject = $.parseJSON(modalSubject);
                console.log(modalSubject[0]);
                var modalInfo = modalSubject[0];
                $(".modalHeader h3").html(modalInfo.question);
                $('#modalUserAndTime').html(modalInfo.username + " | " + modalInfo.time);
                $('#modalCodeNameLector').html(modalInfo.code + " | " + modalInfo.name + " | " + modalInfo.lector);
            });
            $.get("../database/getAnswers.php", {term: modalID}).done(function (answers) {
                $('#modalAnswer').empty();
                if (answers.length > 0) {
                    answers = $.parseJSON(answers);
                    for (var i = 0; i < answers.length; i++) {
                        $('#modalAnswer').append($('<p></p>', {
                            text: answers[i].username + ": " + answers[i].answer
                        }))
                    }
                } else {
                    $('#modalAnswer').append('<b>Vastuseid ei ole. Kas oskad ehk vastata?</b>')
                }
            });
        }
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    };

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }
});
