window.addEventListener('load', function () {
    function getContent() {
        // var queryString = {'timestamp': timestamp};

        $.ajax(
            {
                type: 'GET',
                url: 'https://askut.today/server/server.php',
                data: queryString,
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    $('#response').html(obj.dataFromDB);
                    getContent(obj.timestamp);
                }
            }
        );
    }

    $(function () {
        getContent();
    });
});

