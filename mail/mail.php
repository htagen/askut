<?php

use PHPMailer\PHPMailer\PHPMailer;

require_once 'vendor/autoload.php';
include_once 'mailHelper.php';

function sendWelcomeMail($username, $userMail, $userPassword)
{
    $subject = 'Tere tulemast AskUT-sse!';
    $body = 'Hei, '.$username.'!<br><br>
                Aitäh, et liitusid AskUT-ga. <br>
                Loodame, et leiad meie saidilt kõikidele oma põletavatele küsimustele vastused.<br><br>
                <b>Kasutajanimi: '.$username.'</b><br>
                Juhul kui sul on AskUT tiimile küsimusi või ettepanekuid, 
                võid need meile saata läbi AskUT tagasisidevormi või vastata sellele meilile.<br><br>
                Sinu AskUT tiim!<br>
                askut.today@gmail.com<br>
                https://askut.today';
    $altBody = 'Hei, '.$username.'!
                Aitäh, et liitusid AskUT-ga.
                Loodame, et leiad meie saidilt kõikidele oma põletavatele küsimustele vastused.
                Kasutajanimi: '.$username.'
                Juhul kui sul on AskUT tiimile küsimusi või ettepanekuid, 
                võid need meile saata läbi AskUT tagasisidevormi või vastata sellele meilile.
                Sinu AskUT tiim!
                askut.today@gmail.com
                https://askut.today';

    send($userMail,$subject,$body,$altBody,true);
}
function sendRestorePasswordMail($email, $username, $userPassword)
{
    $altBody = 'Hei, '.$username.'!
                Taastasid AskUT portaalis oma parooli.
                Kasutajanimi: '.$username.'
                Kui sa ei tellinud uut parooli, siis muuda oma parool koheselt AskUT portaalis ning võta meiega ühendust.
                Juhul kui sul on AskUT tiimile küsimusi või ettepanekuid, 
                võid need meile saata läbi AskUT tagasisidevormi või vastata sellele meilile.
                Sinu AskUT tiim!
                askut.today@gmail.com
                https://askut.today';


    $subject = 'Sinu parool on taastatud!';
    $body = 'Hei, '.$username.'!<br><br>
                Taastasid AskUT portaalis oma parooli.<br>
                <b>Kasutajanimi: '.$username.'</b><br>
                Kui sa ei tellinud uut parooli, siis muuda oma parool koheselt AskUT portaalis ning võta meiega ühendust.<br><br>
                Juhul kui sul on AskUT tiimile küsimusi või ettepanekuid, 
                võid need meile saata läbi AskUT tagasisidevormi või vastata sellele meilile.<br><br>
                Sinu AskUT tiim!<br>
                askut.today@gmail.com<br>
                https://askut.today';

    send($email,$subject,$body,$altBody,true);
}
//FIXME uuendab andmed ära, aga saadab meili valesti
function sendUpdateUserInfoMail($email, $username, $userPassword)
{
    $altBody = 'Hei, '.$username.'!
                Uuendasid AskUT portaalis oma andmeid.
                Kasutajanimi: '.$username.'
                Kui sa ei muutnud enda andmeid, siis muuda oma parool koheselt AskUT portaalis ning võta meiega ühendust.
                Juhul kui sul on AskUT tiimile küsimusi või ettepanekuid, 
                võid need meile saata läbi AskUT tagasisidevormi või vastata sellele meilile.
                Sinu AskUT tiim!
                askut.today@gmail.com
                https://askut.today';


    $subject = 'Sinu andmed on uuendatud!';
    $body = 'Hei, '.$username.'!<br><br>
                Uuendasid AskUT portaalis oma andmeid.<br>
                <b>Kasutajanimi: '.$username.'</b><br>
                Kui sa ei muutnud enda andmeid, siis muuda oma parool koheselt AskUT portaalis ning võta meiega ühendust.
                Juhul kui sul on AskUT tiimile küsimusi või ettepanekuid, 
                võid need meile saata läbi AskUT tagasisidevormi või vastata sellele meilile.
                Sinu AskUT tiim!<br>
                askut.today@gmail.com<br>
                https://askut.today';

    send($email,$subject,$body,$altBody,true);
}


function sendMailToAskUT($email, $subject, $messageBody)
{
    send($email,$subject,$messageBody, null,false);
}

function send($email, $subject, $messageBody, $altBody, $sendMailOutBoolean)
{
    $password = 'password'; //askUT gmail parool
    $mail = new PHPMailer;

    $mail->IsSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';                 // Specify main and backup server
    $mail->Port = 587;                                    // Set the SMTP port
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'askut.today@gmail.com';                // SMTP username
    $mail->Password = mailConfig($password);                  // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
    $mail->CharSet = 'UTF-8';

    if(!$sendMailOutBoolean){
        $fromName = 'KASUTAJA: '.$email;
        $toMail = 'askut.today@gmail.com';
    } else {
        $fromName = 'AskUT tiim';
        $toMail = $email;
    }

    $mail->From = 'askut.today@gmail.com';
    $mail->FromName = $fromName;
    $mail->AddAddress($toMail);  // Add a recipient

    $mail->IsHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body = $messageBody;
    $mail->AltBody = $messageBody;

    try {
        if (!$mail->Send()) {
            echo 'Meili saatmine ebaõnnestus!';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            exit;
        }
        else {
            echo '<p class="notice">Teade on saadetud!</p>';
            return true;
        }
    } catch (\PHPMailer\PHPMailer\Exception $e) {
        return false;
    }

    function isShellSafe($string)
    {
        $string = strval($string);
        $length = strlen($string);

        // If you need to allow empty strings, you can remove this, but be sure you
        // understand the security implications of doing so.
        if (!$length) {
            return false;
        }

        // Method 1
        // Note: Results may be indeterminate with a stateful encodings, e.g. EUC
        for ($i = 0; $i < $length; $i++) {
            $c = $string[$i];
            if (!ctype_alnum($c) && strpos('@_-.', $c) === false) {
                return false;
            }
        }
        //return true;

        // Method 2
        // Note: Assumes UTF-8 encoding.  Conversion may be necessary.
        return (bool) preg_match('/\A[\pL\pN._@-]*\z/ui', $string);
    }
}