<?php
include_once 'helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';

// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

//kasutajate arv == COUNT kasutamine
$sqlUserNo = 'SELECT COUNT(*) FROM users';

$resultU = $conn->prepare($sqlUserNo);
$resultU->execute();
$resultU->bind_result($uCount);
$resultU->fetch();
if ($resultU) {
//    $uCount = mysqli_fetch_array($resultU);
    printf("Registreeritud kasutajaid: %d\t • ",$uCount);
 //   mysqli_free_result($resultU);
    $resultU->close();
}

//küsimuste arv:
$sqlQuestionNo = 'SELECT COUNT(*) FROM question';

$resultQ = $conn->prepare($sqlQuestionNo);
$resultQ->execute();
$resultQ->bind_result($qCount);
$resultQ->fetch();
if ($resultQ) {
   // $qCount = mysqli_fetch_array($resultQ);
    printf("Küsitud küsimusi: %d\t",$qCount);
   // mysqli_free_result($resultQ);
    $resultQ->close();
}

////ainete arv:
//$sqlSubjectNo = 'SELECT COUNT(DISTINCT(code)) FROM subject';
//$resultS = $conn->query($sqlSubjectNo);
//if ($resultS) {
//    $sCount = mysqli_fetch_array($resultS);
//    printf("Esindatud aineid: %d\t —  ",$sCount[0]);
//    mysqli_free_result($resultS);
//}
//
////küsituim aine
//$sqlMostPopularSubject = 'SELECT COUNT(question) AS count, code FROM question GROUP BY code ORDER BY COUNT(question) DESC LIMIT 1';
//$resultMP = $conn->query($sqlMostPopularSubject);
//if ($resultMP) {
//    $mpPopular = mysqli_fetch_array($resultMP);
//    $mpSubject = $mpPopular['code'];
//    $mpSAmount = $mpPopular['count'];
//    printf("Enim küsimusi: %s (%u)\t", $mpSubject, $mpSAmount);
//    mysqli_free_result($resultMP);
//}

$conn->close();