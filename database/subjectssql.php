<?php
include_once 'helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';
// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}


$sql = "SELECT DISTINCT subject.code, 
        subject.name, subject.institute, 
        COALESCE(q, 0) as questions 
        FROM subject 
        LEFT JOIN (
        SELECT subject.code, COUNT(question.code) as q 
        FROM subject, question WHERE subject.code = question.code 
        GROUP BY subject.ID) as question_nr 
        ON subject.code = question_nr.code";

$conn->set_charset('utf8');
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo '<table class="tableAskut" id="subjectTable"><tr><th >AINE KOOD</th><th >AINE NIMETUS</th><th >INSTITUUT</th><th >KÜSIMUSI</th></tr>';

    // iga rea väljastamine tabelisse
    while($row = $result->fetch_assoc()) {
        echo '<tr ><td >' . $row['code']. '</td><td >' . $row['name']. ' </td><td > ' . $row['institute']. '</td><td > ' . $row['questions'].  '</td></tr>';
    }
    echo '</table>';
} else {
    echo '0 results';
}
$conn->close();