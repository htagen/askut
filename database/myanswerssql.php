<?php
include_once 'helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';
// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}
$loggedInUser = $_SESSION['username'];

$sql = "SELECT answer.answer, answer.ID as aID, answer.question_ID, answer.username, question.question, question.id, question.code from answer,question 
 WHERE '$loggedInUser'= answer.username and question.id = question_ID";

$conn->set_charset('utf8');
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo '<table class="tableAskut" id="answerTable"><tr ><th>KÜSIMUS</th><th>AINE KOOD</th><th>SINU VASTUS</th><th>x</th></tr>';
    // iga rea väljastamine tabelisse
    while($row = $result->fetch_assoc()) {
        $aID = $row['aID'];
        $_SESSION['id'] = $aID;
        echo '<tr id="'.$aID.'" class="jsMainQuestionButton"><td>' . $row['question']. '</td><td>' . $row['code']. ' </td><td> ' . $row['answer']. '</td><td><form method="post"><button name="delABtn" type="submit" value="<?php if(isset($_POST[\'delABtn\'])) {echo $_SESSION[\'id\'] }?>" class="loginButton deleteQBtn">KUSTUTA</button></form></td></tr>';
    }
    echo '</table>';
} else {
    echo '<p>Sa pole veel ühelegi küsimusele vastanud.</p>';
}
$conn->close();