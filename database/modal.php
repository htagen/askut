<?php
include_once 'helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';
//$questionBoxes = array('q1','q2','q3','q4','q5','q6','q7','q8','q9');

$selectedQuestionBoxId = isset($_POST['id']) ? (int)$_POST['id'] : 1;

// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));
$conn->set_charset('utf8');
// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

//$qID = $selectedQuestionBoxId;

$sqlRecentQuestions = "select * from question where question.ID= '".$selectedQuestionBoxId."' order by time ASC";
$resultQuestions = $conn->query($sqlRecentQuestions);

if ($resultQuestions->num_rows > 0) {
    $index = 0;
    if($resultQuestions->num_rows === 9){
        $index = 9;
    }
    else{
        $index = $resultQuestions->num_rows;
    }
    for ($i = 0; $i < $index; $i++){
        $row = $resultQuestions->fetch_assoc();
        echo '<div id="mainModal" class="modal">
                    <div class="modalContent">
                        <span class="close">&times;</span>
                        <div class="modalHeader">
                            <p id="modalUserAndTime">'.$row['username'].'   |   '.$row['time'].'</p>
                            <h3 id= "modalQuestion">'.$row['question'].'</h3>
                            <p id="modalCodeNameLector">'.$row['code'].'   |   '.$row['name'].' |  Õppejõud: '.$row['lector'].'</p>
                        </div>
                        <div id="modalBody">
                        <div id="modalAnswer"></div>
                        </div>
                        <form method="post" itemscope itemtype="http://schema.org/SearchAction">
                            <div class= "modalFooter">';
                                if (isset($_SESSION['username'])) {
                                    echo '<textarea title="answer" id="modalAnswerArea" name="answer" required placeholder="Vasta" autocomplete="off" oninvalid="this.setCustomValidity(\'Palun täida see väli!\')"
                                            oninput="setCustomValidity(\'\')"></textarea>
                                            <button class="loginButton sendButton" type="submit" name="answerButton">Saada</button>';
                                }
                            echo '</div>
                        </form>
                    </div>
            </div>';
    }
} else echo 'Midagi läks valesti. Proovi mõne aja pärast uuesti!';

$conn->close();
