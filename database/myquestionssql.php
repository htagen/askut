<?php
include_once 'helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';
// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}
$loggedInUser = $_SESSION['username'];

$sql = "SELECT question.id, question.question, question.code, question.name, 
        question.lector, question.time,COALESCE(count_answers, 0) as answers 
        FROM question LEFT JOIN (SELECT COUNT(*) as count_answers from answer 
        GROUP BY question_ID) as answer_nr ON question.ID = answer_nr.count_answers 
        WHERE '$loggedInUser'= question.username ";

$conn->set_charset('utf8');
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo '<table class="tableAskut" id="questionTable"><tr ><th>KÜSIMUS</th><th>AINE KOOD</th><th>AINE NIMETUS</th><th>ÕPPEJÕUD</th ><th>AEG</th><th>VASTUSEID</th><th>x</th></tr>';
    // iga rea väljastamine tabelisse
    while($row = $result->fetch_assoc()) {
        $qID = $row['id'];
        if ($qID != 1) {
            $_SESSION['id'] = $qID;
            echo '<tr id="' . $qID . '" class="jsMainQuestionButton"><td>' . $row['question'] . '</td><td>' . $row['code'] . ' </td><td> ' . $row['name'] . '</td><td> ' . $row['lector'] . ' </td><td> ' . $row['time'] . ' </td><td> ' . $row['answers'] . ' </td><td><form method="post"><button name="delBtn" type="submit" value="<?php if(isset($_POST[\'delBtn\'])) {echo $_SESSION[\'id\'] }?>" class="loginButton deleteQBtn">KUSTUTA</button></form></td></tr>';
        }
    }
    echo '</table>';
} else {
    echo '<p>Sa pole veel ühtegi küsimust küsinud.</p>';
}
$conn->close();