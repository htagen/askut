<?php
include_once 'helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';
// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}