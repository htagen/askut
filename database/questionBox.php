<?php

include_once '../database/helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';
// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));


$user = "";
$pictureUrl = "";

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

$sql = 'SELECT question, question.username, code, question.ID as "qID", question.time as "qTime", count(answer) as "answerNr" 
        from question 
        left join answer 
        on question.ID=question_ID
        GROUP BY question.ID 
        union 
        SELECT question, question.username, code, question.ID as "qID", question.time as "qTime", count(answer) as "answerNr"  
        from question 
        right join answer 
        on question.ID=question_ID 
        GROUP BY question.ID 
        ORDER BY qTime desc limit 10';

$conn->set_charset('utf8');
$queryResult = $conn->query($sql) or trigger_error("Query Failed! SQL: $sql - Error: ".mysqli_error($conn), E_USER_ERROR);

if ($queryResult === false){
    echo 'Midagi läks valesti. Proovi mõne aja pärast uuesti!';
}else if ($queryResult->num_rows > 0) {
    // output data of each row

    $qBoxId = array('q1','q2','q3','q4','q5','q6','q7','q8','q9');
    $index = 0;
    $result = new \stdClass();

    while($row = $queryResult->fetch_assoc()) {
        if($index === 10){ break;}
        $int = $row['qID'];
        if ($int != 1) {
            $questionText = $row['question'];
            if (strlen($questionText) > 40) {
                $questionCut = substr($questionText, 0, 40);
                $questionText = $questionCut . '...  <span class="readMoreLink">Loe veel</span>';
            }

            $user = $row['username'];

            $sql2 = "SELECT url from users where username = '$user'";
            $queryResult2 = $conn->query($sql2) or trigger_error("Query Failed! SQL: $sql2 - Error: " . mysqli_error($conn), E_USER_ERROR);

            if ($queryResult2 === false) {
                $pictureUrl = "defaultavatar.png";
            } else if ($queryResult2->num_rows > 0) {
                $pictureUrl = $queryResult2->fetch_assoc()['url'];
            }

            //teeme modali jaoks iga küsimuse kohta globaalsed muutuja, et modal saaks alati mainil kuvatava sisu (mitte reaalajas kõige õigema)
//        $_GET[$qBoxId[$index]] = $row['qID'];
            $id = $qBoxId[$index];

            echo '<div class="mainQuestionButton jsMainQuestionButton" onclick="passVal(' . $int . ')" id="' . $int . '">';
            echo '<div class="qBoxContent">';
            echo '<img itemprop="image" class="qBoxAvatar" src="../avatars/' . $pictureUrl . '" alt="AskUT kasutaja profiilipilt">';
            echo '<div class="qBoxQContainer">';
            echo '<p class="qBoxQuestion">' . $questionText . '</p>';
            echo '</div>';
            echo '</div>';
            echo '<div class="qBoxFooter"><hr class="qBoxHr"><div class="qBoxAnswerNr">Vastuseid: ' . $row['answerNr'] . '</div><div class="qBoxFooterText">' . $row['code'] . '</div></div>';
            echo '</div>';
            $index++;
        }
    }
}

$conn->close();
