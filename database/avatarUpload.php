<?php
if (isset($_POST['submit'])) {
    $file = $_FILES['fileToUpload'];

    $fileName = $file['name'];
    $fileTmpName = $file['tmp_name'];
    $fileSize = $file['size'];
    $fileError = $file['error'];
    $fileType = $file['type'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('jpg', 'jpeg', 'png', 'gif');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 500000) {
                $fileNameNew = uniqid('', true).".".$fileActualExt;
                $uploadDir = realpath('/var/www/html/avatars/');
                $uploadFile = $uploadDir . "/" . $fileNameNew;
                if (move_uploaded_file($fileTmpName, $uploadFile)) {

                    session_start();
                    include_once '../database/helper.php';
                    include_once '../database/server.php';
                    $servername = "hostname";
                    $username = "username";
                    $password = "password";
                    $dbname = "name";

                    // Andmebaasiga ühendamine
                    $conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

                    // Andmebaasi ühenduse kontroll
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }
                    $username = $_SESSION['username'];

                    //võtame vana avatari asukoha andmebaasist
                    $sqlOldAvatar = "SELECT url FROM users WHERE users.username = ?";
                    $resultOldAvatar = $conn->prepare($sqlOldAvatar);
                    $resultOldAvatar->bind_param('s',$username);
                    $resultOldAvatar->execute();
                    $resultOldAvatar->bind_result($oldAvatar);
                    $resultOldAvatar->fetch();

                    //$resultOldAvatar = $connection->query($sqlOldAvatar);
                    if ($resultOldAvatar === false) {
                        echo "Error: " . $sql . "<br>" . $connection->error;
                    } else {
                  //      $oldAvatar = mysqli_fetch_array($resultOldAvatar);
                        $oldAvatarPath = "../avatars/" . $oldAvatar;
                        //mysqli_free_result($resultOldAvatar);
                        $resultOldAvatar->close();
                      //  $sqlAvatarUpload = "UPDATE users SET users.urlstatus = 1, users.url = '$fileNameNew' WHERE users.username = '$username'";
                        $sqlAvatarUpload = "UPDATE users SET users.urlstatus = 1, users.url = ? WHERE users.username = ?";
                        $sqlAvatarUploadResult = $conn->prepare($sqlAvatarUpload);
                        $sqlAvatarUploadResult->bind_param('ss', $fileNameNew, $username);
                        $sqlAvatarUploadResult->execute();
                        $sqlAvatarUploadResult->fetch();

                        //kõik läks hästi->kustutame vana avatari serverist
                        if ($sqlAvatarUploadResult === TRUE) {
                            if ($oldAvatarPath != "../avatars/defaultavatar.png") {
                                unlink($oldAvatarPath);
                            }
                        } else {
                            echo "Error: " . $sqlAvatarUpload . "<br>" . $conn->error;
                        }
                        $conn->close();
                    }
                    header("location: http://askut.today/home/profile.php?success");
                } else {
                    header("location: http://askut.today/home/profile.php?failure");
                    echo "<script>alert('Profiilipilti ei uuendatud.'); window.location='../home/profile.php'</script>";
                }
            } else {
                echo "<script>alert('Fail on liiga suur. Maksimaalne suurus 500kB.'); window.location='../home/profile.php'</script>";
            }
        } else {
            echo "<script>alert('Midagi läks valesti. Palun proovi uuesti.'); window.location='../home/profile.php'</script>";
        }
    } else {
        echo "<script>alert('AskUT ei toeta sellist tüüpi avatari. Lubatud: jpg, jpeg, png, gif'); window.location='../home/profile.php'</script>";

    }
}