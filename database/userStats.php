<!--Leiame ja salvestame kasutajastatistika-->
<?php
include_once 'helper.php';

//Leiame kasutaja IP, brauseri, OP-süsteemi ja kellaaja.
$browserArray = get_browser();
$userIp = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
$userBrowser = $browserArray->browser;
$userOs = $browserArray->platform;
date_default_timezone_set('Europe/Tallinn');
$time = date("H");

//Salvestame andmebaasi, kui täpselt sellist rida juba ei eksisteeri
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';

// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

//Kontrollime, kas täpselt selliste väärtustega rida on juba stats tabelis olemas
$sqlStatsExist = "SELECT * FROM stats WHERE ip='$userIp' AND browser='$userBrowser' AND os='$userOs' AND time='$time'";
$resultExists = $conn->query($sqlStatsExist);
//Sellist rida ei ole veel tabelis. Sisestame selle.
if (mysqli_num_rows($resultExists) < 1 || !$resultExists) {
    $query = "INSERT INTO stats (ip, browser, os, time) VALUES ('$userIp', '$userBrowser', '$userOs', '$time')";
    // Tabelisse sisestamise päring
    try {
        mysqli_query($conn, $query);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
} mysqli_free_result($resultExists);

