<?php
//Suhtlus serveriga
ini_set('display_errors',1);
error_reporting(E_ALL);

include_once 'helper.php';
include_once '../mail/mail.php';
include_once '../facebook/facebook-php-sdk/src/Facebook/autoload.php';
//include_once '../loginphp/client.php';

// Andmebaasiga ühendamine
$servername = "hostname";
$username = "username";
$password = "password";
$dbname = "name";

$connection = mysqli_connect(config($servername), config($username), config($password), config($dbname));
// Andmebaasi ühenduse kontroll
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}

//määrame algseteks kasutajanimeks ja emailiks tühja sõne, et saaks pärast kontrollida, kas kasutaja sisestas sinna midagi
$username = "";
$email = "";
$passwdHash = "";

$options = [
    'cost' => 13,
];

//kasutaja jm errorite jaoks
$errors = array();

if (isset($_POST['register'])) {
    // Vormist kasutaja sisestatud andmete saamine
    $username = mysqli_real_escape_string($connection, $_POST['username']);
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $course = mysqli_real_escape_string($connection, $_POST['course']);
    $institute = mysqli_real_escape_string($connection, $_POST['institute']);
    $password = mysqli_real_escape_string($connection, $_POST['password']);
    $repeatPassword = mysqli_real_escape_string($connection, $_POST['repeatPassword']);

    //kontrollime kas väljad said kõik ilusti täidetud, kui ei, siis anname errori järjendisse, mis puudu või valesti
    if (empty($username)) {
        array_push($errors, "Kasutajanimi on vajalik!");
    }
    if (empty($email)) {
        array_push($errors, "Email on vajalik!");
    }
    if (empty($password)) {
        array_push($errors, "Parool on vajalik!");
    }
    if ($password != $repeatPassword) {
        array_push($errors, "Paroolid ei klapi omavahel");
    }
    if (strlen($username) < 4) {
        array_push($errors, "Kasutajanime pikkus peab olema pikem kui 4 tähte");
    }
    if (preg_match('/[^A-ZÕÄÖÜa-zõäöü0-9]+/',$username)){
        array_push($errors, "Kasutajanimi võib koosneda ainult tähtedest ja numbritest!");
    }
    if (strlen($password) < 8) {
        array_push($errors, "Parooli pikkus peab olema pikem kui 8 tähte");
    }

    //Kontrollime, et andmebaasis sellist kasutajat meil juba ei oleks
    $userCheck = "Select * From users WHERE username='$username' OR email='$email' Limit 1";
    $result = mysqli_query($connection, $userCheck);
    $user = mysqli_fetch_assoc($result);

    //kui kasutaja on olemas
    if ($user) {
        if ($user['username'] === $username) {
            array_push($errors, "Kasutajanimi on juba olemas!");
        }
        if ($user['email'] === $email) {
            array_push($errors, "Email on juba kasutusel!");
        }
    }

    //Kasutaja registreerimine, sisestamine tabelisse juhul kui erroreid pole
    if (count($errors) == 0) {

        //krüpteerime parooli
        $password = password_hash($password,PASSWORD_BCRYPT, $options);

        $query = "INSERT INTO users (username, email, course, institute, password) VALUES ('$username', '$email', '$course', '$institute', '$password')";

        // Tabelisse sisestamise päring
        try {
            mysqli_query($connection, $query);
            //sisseloginud kasutajate jälgimiseks, et teada kas ja kes sisseloginud on
           // $_SESSION['username'] = $username;
            //automaatteavitus õnnestunud registreerimise kohta:
            sendWelcomeMail($username,$email,$password);
            echo '<p class="noticeSuccess">Kasutaja registreeritud!</p>';
//            header('location: login.php');

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}


//Kasutaja sisse logimine
if (isset($_POST['loginUser'])) {
    $username = mysqli_real_escape_string($connection, $_POST['username']);
    $password = mysqli_real_escape_string($connection, $_POST['password']);

    if (count($errors) == 0) {
        //Mysql lause, millega võrdleme kas sellist kasutajat, sellise parooliga on olemas
        $query = "SELECT password FROM users WHERE username='$username'";
            $results = mysqli_query($connection, $query);
            if($results->num_rows != 1){
                array_push($errors, "Sellist kasutajat ei eksisteeri.");
            }
            else {
                while ($obj = mysqli_fetch_object($results)) {
                    $passwdHash = $obj->password;
                }

                //kui leitud kasutajat on täpselt üks, siis logime sisse
                if (password_verify($password, $passwdHash)) {
                    $_SESSION['username'] = $username;
                    //Suuname kasutaja sinna lehele, kuhu ta eelnevalt soovis, võte 9.
                    if (isset($_SESSION['url'])) {
                        $url = $_SESSION['url'];
                    } else {
                        $url = "/home/main.php";
                    }
                    header("Location: http://askut.today$url");

                } else {
                    array_push($errors, "Vale parool!");
                }
        }
    }

}

//Kasutaja logib sisse ID-kaardiga. Vaatame kas sellist kontot on juba olemas

if (isset($_SESSION['idUserId'])) {
    $userIdCode = $_SESSION['idUserId'];
    $userEmail = $_SESSION['idUserEmail'];

    //Kas selline kasutaja andmebaasis
    $queryIdCard = "SELECT idcardid FROM users WHERE idcardid = $userIdCode";
    $resultIdCard = $connection->query($queryIdCard);
    if ($resultIdCard) {
        $idCard = mysqli_fetch_array($resultIdCard)[0];
        mysqli_free_result($resultIdCard);
    }
    //Kui sellist kasutajat andmebaasist ei leidnud
    if (!isset($idCard)) {
        //Kasutaja email on märgitud -> kontrollime, kas selline email on meie andmebaasis
        if (isset($userEmail)) {
            $queryIdCard = "SELECT email FROM users WHERE email = '$userEmail'";
            $resultIdEmail = $connection->query($queryIdCard);
            if ($resultIdEmail) {
                $idEmail = mysqli_fetch_array($resultIdEmail)[0];
                mysqli_free_result($resultIdEmail);
            }
            //Selline email on andmebaasis -> määrame SESSIONi kasutajanimeks selle kasutajanime
            if (isset($idEmail)) {
                $queryEmailUser = "SELECT username FROM users WHERE email = '$userEmail'";
                $resultsEmailUser = $connection->query($queryEmailUser);
                if ($resultsEmailUser) {
                    $emailUser = mysqli_fetch_array($resultsEmailUser)[0];
                    $_SESSION['username'] = $emailUser;
                    mysqli_free_result($resultsEmailUser);
                }
            } //Sellist emaili ei ole andmebaasis -> loome uue kasutaja: [userx, email, idcardid]
            else {
                $newUsername = newUsername();
                try {
                    $queryAddUser = "INSERT INTO users (username, email, idcardid) VALUES ('$newUsername', '$userEmail', '$userIdCode')";
                    if ($connection->query($queryAddUser) === TRUE) {
                        $_SESSION['username'] = $newUsername;
                    } else {
                        echo "Error: " . $queryAddUser . "<br>" . $connection->error;
                    }
                } catch (mysqli_sql_exception $e) {
                    echo $e->getMessage();
                }
            }
        }
    } //Selline ID-kaardi idcode on andmebaasis -> määrame SESSIONi kasutajanimeks selle kasutajanime
    else {
        $queryUsername = "SELECT username FROM users WHERE idcardid = '$userIdCode'";
        $resultsUsername = $connection->query($queryUsername);
        if ($resultsUsername) {
            $idUsername = mysqli_fetch_array($resultsUsername)[0];
            $_SESSION['username'] = $idUsername;
            mysqli_free_result($resultsUsername);
        }
    }
}

//Kasutaja logib sisse Facebookiga. Uurime, kas tema fbID või emailiga on juba konto olemas.
if (isset($_SESSION['fbUserId'])) {
    $userId = $_SESSION['fbUserId'];
    $userEmail = $_SESSION['fbUserEmail'];

    //Uurime, kas selline fbId on andmebaasis
    $queryFbId = "SELECT fbid FROM users WHERE fbid = $userId";
    $resultFbId = $connection->query($queryFbId);
    if ($resultFbId) {
        $fbId = mysqli_fetch_array($resultFbId)[0];
        mysqli_free_result($resultFbId);
    }

    //Sellist fbID'd ei ole andmebaasis
    if (!isset($fbId)) {
        //Kasutaja email on märgitud->kontrollime, kas selline email on meie andmebaasis
        if (isset($userEmail)) {
            $queryFbEmail = "SELECT email FROM users WHERE email = '$userEmail'";
            $resultsFbEmail = $connection->query($queryFbEmail);
            if ($resultsFbEmail) {
                $fbEmail = mysqli_fetch_array($resultsFbEmail)[0];
                mysqli_free_result($resultsFbEmail);
            }
            //Selline email on andmebaasis->määrame SESSIONi kasutajanimeks selle kasutajanime
            if (isset($fbEmail)) {
                $_SESSION['issetID'] = "siin0";
                $queryEmailUser = "SELECT username FROM users WHERE email = '$userEmail'";
                $resultsEmailUser = $connection->query($queryEmailUser);
                if ($resultsEmailUser) {
                    $_SESSION['issetID'] = "siin1";
                    $emailUser = mysqli_fetch_array($resultsEmailUser)[0];
                    $_SESSION['username'] = $emailUser;
                    mysqli_free_result($resultsEmailUser);
                }
            } //Sellist emaili ei ole andmebaasis->loome uue kasutaja: [userx, email, fbid]
            else {
                $newUsername = newUsername();
                try {
                    $queryAddUser = "INSERT INTO users (username, email, fbid) VALUES ('$newUsername', '$userEmail', '$userId')";
                    if ($connection->query($queryAddUser) === TRUE) {
                        $_SESSION['username'] = $newUsername;
                    } else {
                        echo "Error: " . $queryAddUser . "<br>" . $connection->error;
                    }
                } catch (mysqli_sql_exception $e) {
                    echo $e->getMessage();
                }
            }
        } //Kasutaja email ei ole märgitud->loome uue kasutaja: [userx, fbid]
        else {
            $newUsername = newUsername();
            try {
                $queryAddUser = "INSERT INTO users (username, fbid) VALUES ('$newUsername','$userId')";
                if ($connection->query($queryAddUser) === TRUE) {
                    $_SESSION['username'] = $newUsername;
                } else {
                    echo "Error: " . $queryAddUser . "<br>" . $connection->error;
                }
            } catch (mysqli_sql_exception $e) {
                echo $e->getMessage();
            }
        }
    } //Selline fbID on andmebaasis->määrame SESSIONi kasutajanimeks selle kasutajanime
    else {
        $queryUsername = "SELECT username FROM users WHERE fbid = '$userId'";
        $resultsUsername = $connection->query($queryUsername);
        if ($resultsUsername) {
            $fbUsername = mysqli_fetch_array($resultsUsername)[0];
            $_SESSION['username'] = $fbUsername;
            mysqli_free_result($resultsUsername);
        }
    }
}

function newUsername() {
    // Andmebaasiga ühendamine
    $servername = "hostname";
    $username = "username";
    $password = "password";
    $dbname = "name";

    $connection = mysqli_connect(config($servername), config($username), config($password), config($dbname));
    // Andmebaasi ühenduse kontroll
    if ($connection->connect_error) {
        die("Connection failed: " . $connection->connect_error);
    }
    //muutuja, millega hoiame silma peal, kas selline suvaline kasutajanimi juba eksisteerib
    $thisUserExists = true;
    $newUserNo = rand(1, 15000);
    //Kasutaja nimi suvalise numbriga kujul: user + x [nt. user1220]
    $newUsername = "user" . $newUserNo;
    //kontrollime, et täpselt sellist userit ei eksisteeriks juba
    while ($thisUserExists) {
        $queryUserExists = "SELECT username FROM users where username = '$newUsername'";
        $resultUserExists = $connection->query($queryUserExists);
        if ($resultUserExists) {
            $userExists = mysqli_fetch_array($resultUserExists)[0];
            if (isset($userExists)) {
                $newUserNo = rand(1, 15000);
                $newUsername = "user" . $newUserNo;
            } else {
                $thisUserExists = false;
            }
        }
        mysqli_free_result($resultUserExists);
    } return $newUsername;
}

//Kasutaja parooli taastamine
if (isset($_POST['restore'])) {
    $usernameOrEmail = mysqli_real_escape_string($connection, $_POST['username']);

    if (count($errors) == 0) {

        $randomPasswd = str_shuffle(bin2hex(openssl_random_pseudo_bytes(4))); //teeme uue random 8 märgi pikkuse parooli
        $randomPasswordHash = password_hash($randomPasswd,PASSWORD_BCRYPT, $options);

        $query = "update users set password='$randomPasswordHash' WHERE username='$usernameOrEmail' or email='$usernameOrEmail'";
        mysqli_query($connection, $query);

        if(mysqli_affected_rows($connection)) {
            $query = "select username, email from users where username='$usernameOrEmail' or email='$usernameOrEmail'";
            $results = mysqli_query($connection, $query);
            $user = mysqli_fetch_assoc($results);
            sendRestorePasswordMail($user['email'], $user['username'], $randomPasswd);
            header("Location: http://askut.today/home/login.php");
        }
        else {
            array_push($errors, "Sellist kasutajat ei eksisteeri.");
        }
    }
}

//Kasutaja meili AskUT'le saatmine:
if (isset($_POST['sendMail'])) {
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $subject = mysqli_real_escape_string($connection, $_POST['subject']);
    $message = mysqli_real_escape_string($connection, $_POST['message']);

    if(sendMailToAskUT($email, $subject, $message)){
        echo 'Kiri saadetud!';
    }
}

//Kasutaja avatar
if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
    $sqlGetAvatar = "SELECT url FROM users WHERE users.username = '$username'";
    $resultAvatar = $connection->query($sqlGetAvatar);
    if ($resultAvatar === false) {
        echo "Error: " . $sqlGetAvatar . "<br>" . $connection->error;
    } else {
        $avatar = mysqli_fetch_array($resultAvatar);
        $_SESSION['userAvatar'] = "../avatars/" . $avatar[0];
        mysqli_free_result($resultAvatar);
    }
}

//kasutaja email
if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
    $sqlGetEmail = "SELECT email FROM users WHERE users.username = '$username'";
    $resultEmail = $connection->query($sqlGetEmail);
    if ($resultEmail === false) {
        echo "Error: " . $sqlGetEmail . "<br>" . $connection->error;
    } else {
        $email = mysqli_fetch_array($resultEmail);
        $_SESSION['email'] = $email[0];
        mysqli_free_result($resultEmail);
    }
}
//kasutaja instituut

if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
    $sqlGetInstitute = "SELECT institute FROM users WHERE users.username = '$username'";
    $resultInstitute = $connection->query($sqlGetInstitute);
    if ($resultInstitute === false) {
        echo "Error: " . $sqlGetInstitute . "<br>" . $connection->error;
    } else {
        $institute = mysqli_fetch_array($resultInstitute);

        $_SESSION['institute'] = $institute[0];
        mysqli_free_result($resultInstitute);
    }
}


//kasutaja eriala

if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
    $sqlGetCourse = "SELECT course FROM users WHERE users.username = '$username'";
    $resultCourse = $connection->query($sqlGetCourse);
    if ($resultCourse === false) {
        echo "Error: " . $sql . "<br>" . $connection->error;
    } else {
        $course = mysqli_fetch_array($resultCourse);
        $_SESSION['course'] = $course[0];
        mysqli_free_result($resultCourse);
    }
}


//Kasutaja andmete uuendamine
if (isset($_POST['save'])) {
    $usernameFromForm = mysqli_real_escape_string($connection, $_POST['username']);
    $usernameLoggedIn = $_SESSION['username'];
    $emailLoggedIn = $_SESSION['email'];
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $course = mysqli_real_escape_string($connection, $_POST['course']);
    $institute = mysqli_real_escape_string($connection, $_POST['institute']);
    $existingPassword = mysqli_real_escape_string($connection, $_POST['existingPassword']);
    $newPassword = mysqli_real_escape_string($connection, $_POST['newPassword']);
    $repeatPassword = mysqli_real_escape_string($connection, $_POST['confirmNewPassword']);
    //kontrollime kas väljad said kõik ilusti täidetud, kui ei, siis anname errori järjendisse, mis puudu või valesti
    if (empty($usernameFromForm)) {
        array_push($errors, "Kasutajanimi on vajalik!");
    }
    if (empty($email)) {
        array_push($errors, "Email on vajalik!");
    }
    $userCheck = "Select password From users WHERE username='$usernameLoggedIn'  Limit 1";
    $result = mysqli_query($connection, $userCheck);
    $user = mysqli_fetch_assoc($result);
    if($result->num_rows == 1){
        if (empty($existingPassword) && $user['password'] != '' ){
            array_push($errors, "Parool on vajalik!");
        }
    }

    if ($user['password'] == '' && !empty($existingPassword) ){
        array_push($errors, "Sul pole sellel kasutajal parooli. Jäta praeguse parooli väli tühjaks! ");
    }
    if ($newPassword != $repeatPassword) {
        array_push($errors, "Paroolid ei klapi omavahel");
    }
    $query = "SELECT password FROM users WHERE username='$username'";
    $results = mysqli_query($connection, $query);
    while ($obj = mysqli_fetch_object($results)) {
        $passwdHash = $obj->password;
    }
    if (!password_verify($existingPassword, $passwdHash) && $passwdHash != '') {
            array_push($errors, "Vale parool!");
    }

    else {
        //Kontrollime, et andmebaasis sellist kasutajat meil juba ei oleks
        $userCheck = "Select username From users WHERE username='$usernameFromForm'  Limit 1";
        $userEmailCheck = "Select email From users WHERE email='$email'Limit 1";

        $result = mysqli_query($connection, $userCheck);
        $resultUserEmail = mysqli_query($connection, $userEmailCheck);

        $user = mysqli_fetch_assoc($result);
        $userEmailResult = mysqli_fetch_assoc($resultUserEmail);

        if($result->num_rows == 1 || $resultUserEmail->num_rows == 1){

            if ($user['username'] != $usernameLoggedIn) {
                if ($user['username'] === $usernameFromForm) {
                    array_push($errors, "Kasutajanimi on juba olemas!");
                }
            }
            if ($userEmailResult['email'] != $emailLoggedIn){
                if ($userEmailResult['email'] === $email){
                    array_push($errors, "Email on juba kasutusel!");
                }
            }
        }

        //Kasutaja andmete muutmine, juhul kui erroreid pole
        if (count($errors) == 0) {
            if (!empty($newPassword) && !empty($repeatPassword)) {


                //krüpteerime parooli
                $password = password_hash($newPassword, PASSWORD_BCRYPT, $options);

                $query = "UPDATE users set username='$usernameFromForm', email='$email', course='$course', institute='$institute', password='$password' WHERE username ='$usernameLoggedIn'";
                // Tabelisse sisestamise päring
                try {
                    mysqli_query($connection, $query);
                    //sisseloginud kasutajate jälgimiseks, et teada kas ja kes sisseloginud on
                    $_SESSION['username'] = $usernameFromForm;
                    //automaatteavitus õnnestunud registreerimise kohta:
                    sendUpdateUserInfoMail($email, $usernameFromForm, $newPassword);
                    header('location: profile.php');

                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else { //kui parooli ei tahetud muuta

                $query = "UPDATE users set username='$usernameFromForm', email='$email', course='$course', institute='$institute' WHERE username ='$usernameLoggedIn'";
                // Tabelisse sisestamise päring
                try {
                    mysqli_query($connection, $query);
                    //sisseloginud kasutajate jälgimiseks, et teada kas ja kes sisseloginud on
                    $_SESSION['username'] = $usernameFromForm;
                    //automaatteavitus õnnestunud registreerimise kohta:
                    sendUpdateUserInfoMail($email, $usernameFromForm, $newPassword);
                    header('location: profile.php');

                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
    }
}
//kasutaja kustutamine

if (isset($_POST['deleteUser'])) {
    $loggedInUser = $_SESSION['username'];
    $existingPassword = mysqli_real_escape_string($connection, $_POST['existingPassword']);
    $userCheck = "Select password From users WHERE username='$loggedInUser'  Limit 1";
    $result = mysqli_query($connection, $userCheck);
    $user = mysqli_fetch_assoc($result);
    if ($result->num_rows == 1) {
        if (empty($existingPassword) && $user['password'] != '') {
            array_push($errors, "Parool on vajalik!");
        }
    }
    $query = "SELECT password FROM users WHERE username='$loggedInUser'";
    $results = mysqli_query($connection, $query);
    while ($obj = mysqli_fetch_object($results)) {
        $passwdHash = $obj->password;
    }
    if (!password_verify($existingPassword, $passwdHash) && $passwdHash != '') {
        array_push($errors, "Vale parool!");
    }

    if ($user['password'] == '' && !empty($existingPassword)) {
        array_push($errors, "Sul pole sellel kasutajal parooli. Jäta praeguse parooli väli tühjaks! ");
    }
    if (count($errors) == 0) {

        $query = "select id from users where username='$loggedInUser'";
        $results = mysqli_query($connection, $query);
        $user = mysqli_fetch_assoc($results);
        $loggedInUserID = $user['id'];
        $query = "DELETE FROM users where id='$loggedInUserID'";
        $results = mysqli_query($connection, $query);
        session_destroy();
        session_unset();
        unset($_SESSION['username']);
        unset($_SESSION['fbUserId']);
        unset($_SESSION['idUserId']);
        header("location: https://askut.today");
    }
}

if (isset($_POST['ask'])) {

    //et ü,õ,ä,ü ja muud vahvad tähed ilusti näha oleks
    mysqli_set_charset($connection, "utf8");

    $username = $_SESSION['username'];
    //Kasutaja küsimuse andmed
    $question = mysqli_real_escape_string($connection, $_POST['question']);
    $subject = mysqli_real_escape_string($connection, $_POST['subject']);
    $code = mysqli_real_escape_string($connection, $_POST['code']);
    //TODO: aksepteerime tühja.. kas nii on okei?
    $lector = mysqli_real_escape_string($connection, $_POST['lector']);

    if (empty($question)) {
        array_push($errors, "Küsimus on vajalik!");
    }
    if (empty($subject)) {
        array_push($errors, "Õppeaine nimi on vajalik!");
    }
    if (empty($code)) {
        array_push($errors, "Ainekood on vajalik!");
    }

    //Küsimuse lisamine, kui erroreid ei ole
    if (count($errors) == 0) {

        $query = "INSERT INTO question (username, question, code, name, lector) VALUES ('$username', '$question', '$code', '$subject','$lector')";

        // Tabelisse sisestamise päring
        try {
            //kontrollime, et andmebaasis samal ainel juba sellist küsimust ei oleks
            $questionCheck = "SELECT * FROM question WHERE question.question='$question' AND question.name='$subject' LIMIT 1";
            $result = mysqli_query($connection, $questionCheck);
            $subject = mysqli_fetch_assoc($result);

            //kui küsimus on olemas
            if ($subject) {
                array_push($errors, "Sellel ainel on juba selline küsimus küsitud! Proovi seda otsida.");
            } else {
                $insertNewQuestion = $connection->query($query);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    unset($_POST['ask']);
}

if (isset($_POST['answerButton'])) {

    //et ü,õ,ä,ü ja muud vahvad tähed ilusti näha oleks
    mysqli_set_charset($connection, "utf8");

    $username = $_SESSION['username'];
    //Kasutaja vastuse andmed
    $answer = mysqli_real_escape_string($connection, $_POST['answer']);
    //FIXME: võta hetkese modali ID (peab toimima nii otsides kui ka küsides, hetkel ID lahendus ainult top9 modalil?

    if (empty($username)) {
        array_push($errors, "Vastus on vajalik!");
    }

    //Vastuse lisamine, kui erroreid ei ole
    if (count($errors) == 0 && isset($_SESSION['modalId'])) {
        $id = $_SESSION['modalId'];

        $query = "INSERT INTO answer (username, answer, question_ID) VALUES ('$username', '$answer', '$id')";

        // Tabelisse sisestamise päring
        try {
            $insertNewAnswer = $connection->query($query);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    unset($_POST['answerButton']);
    unset($_SESSION['modalId']);
}


if (isset($_POST['delBtn'])) {
    $qID = $_SESSION['id'];

    $query = "delete from question where ID = '$qID'";
    $results = mysqli_query($connection, $query);

    $query2 = "delete from answer where question_ID = '$qID'";
    $results2 = mysqli_query($connection, $query2);
}

if (isset($_POST['delABtn'])) {
    $aID = $_SESSION['id'];

    $query2 = "delete from answer where answer.ID = '$aID'";
    $results2 = mysqli_query($connection, $query2);
}