
<!--
    see fail oli ainult SQL join võtte jaoks. reaalselt seda kusagil kasutusel (enam) ei ole
-->


<?php
include_once 'helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';
// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

$sql = 'select question, lector, question.name,subject.institute, time from question join subject on subject.code = question.code';
$conn->set_charset('utf8');
$result = $conn->query($sql);
if ($result === false){
    echo 'pekkis';
}
else if ($result->num_rows > 0) {
    echo '<table class="tableAskut" id="joinTable"><tr ><th >Küsimus</th><th >Õppejõud</th><th >Aine nimetus</th><th >Instituut</th><th >Aeg</th></tr>';
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo '<tr ><td >' . $row['question']. '</td><td >' . $row['lector']. ' </td><td > ' . $row['name']. '</td><td > ' . $row['institute']. ' </td><td > ' . $row['time']. ' </td></tr>';
    }
    echo '</table>';
} else {
    echo '0 results';
}

$conn->close();