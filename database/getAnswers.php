<?php
include '../database/helper.php';

ini_set('display_errors', 1);
error_reporting(E_ALL);

$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';

// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));
//ÖÜÄÕ ja muud special characterid kuvatakse/ on otsitavad
mysqli_set_charset($conn, "utf8");

// Check connection
if($conn === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

if(isset($_REQUEST['term'])){

    $sql = "SELECT * FROM answer WHERE answer.question_ID LIKE ?";

    if($stmt = mysqli_prepare($conn, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "s", $param_term);

        // Set parameters
        $param_term = '%' . $_REQUEST['term'] . '%';

        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);

            // Check number of rows in the result set
            if(mysqli_num_rows($result) > 0){
                $results = [];
                // Fetch result rows as an associative array
                while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                    $results[] = $row;
                }
                echo json_encode($results);
            }
        } else{
            echo "ERROR: $sql. " . mysqli_error($conn);
        }
    }

    // Close statement
    mysqli_stmt_close($stmt);
}

// close connection
mysqli_close($conn);
