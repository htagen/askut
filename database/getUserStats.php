<?php

include_once 'helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';

// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

//Brauser
$sqlBrowser = "SELECT browser, COUNT(*) AS count FROM stats GROUP BY browser ORDER BY count DESC";
$resultB = $conn->query($sqlBrowser);
if ($resultB) {
    $i = 1;
    echo "Brauserid:" . "<br />";
    while($row = mysqli_fetch_array($resultB)) {
        if ($row[0] != NULL && $row[1] != NULL) {
            echo $i . ". " . $row['browser'] . " - " . $row[1];
            echo "<br />";
            $i += 1;
        }
    }
    mysqli_free_result($resultB);
    echo "<br />";
}

//OP-süsteem
$sqlOS = "SELECT os, COUNT(*) AS count FROM stats GROUP BY os ORDER BY count DESC";
$resultOS = $conn->query($sqlOS);
if ($resultOS) {
    $i = 1;
    echo "OP-süsteemid:" . "<br />";
    while($row = mysqli_fetch_array($resultOS)) {
        if ($row[0] != NULL && $row[1] != NULL) {
            echo $i . ". " . $row['os'] . " -  " . $row[1];
            echo "<br />";
            $i += 1;
        }
    }
    mysqli_free_result($resultOS);
    echo "<br />";
}

//Populaarsed kellaajad
$sqlTime = "SELECT time, COUNT(*) AS count FROM stats GROUP BY time ORDER BY count DESC";
$resultT = $conn->query($sqlTime);
if ($resultT) {
    $i = 1;
    echo "Populaarsed külastusajad:" . "<br />";
    while($row = mysqli_fetch_array($resultT)) {
        if ($row[0] != NULL && $row[1] != NULL) {
            $rTime = $row['time'];
            $rTimeO = $rTime + 1;
            echo $i . ". " . $rTime . ".00-" . $rTimeO . ".00 - " . $row[1];
            echo "<br />";
            $i += 1;
        }
    }
    mysqli_free_result($resultT);
}