﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
                xmlns="http://www.w3.org/1999/xhtml" xmlns:ut="http://www.ut.ee"
>
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="/*[1]"/>
  </xsl:template>
  
  <xsl:template match="/*[1]">
    <div id="courseinfo">
      <h2>
        <xsl:value-of select="concat(./@kood, ' ', ./@nimi)"/>
      </h2>
      <ol>
        <xsl:apply-templates select="./ut:teemad/ut:teema"/>
      </ol>
      <p>
        <xsl:text>Loengu toimumine: </xsl:text>
        <xsl:value-of select="concat(./ut:loeng/@päev, ' ', ./ut:loeng/@kell)"/>
        <br/>
        <xsl:text>Ruum: </xsl:text>
        <xsl:value-of select="./ut:loeng/@ruum"/>
        <br/>
        <xsl:text>Allikas: XML</xsl:text>
      </p>
    </div>
  </xsl:template>

  <xsl:template match="ut:teema">
    <li>
      <xsl:value-of select="."/>
    </li>
  </xsl:template>

</xsl:stylesheet>
