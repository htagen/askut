﻿window.addEventListener("load", load_Document);
if (typeof window.opera != "undefined")
    window.opera.addEventListener("load", load_Document);

var ROOT_URI = "https://askut.today/ajax/test/";
var xsl = null;

function getRequestObject() {
    if (window.XMLHttpRequest)
        return new window.XMLHttpRequest();
    if (typeof XMLHttpRequest == "undefined") { // old IE
        XMLHttpRequest = function () {
            try { return new ActiveXObject("Msxml2.XMLHTTP.6.0"); } catch (e) { }
            try { return new ActiveXObject("Msxml2.XMLHTTP.3.0"); } catch (e) { }
            try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }
            throw new Error("This browser does not support XMLHttpRequest.");
        };
    }
    else
        return new XMLHttpRequest();
    return XMLHttpRequest();
}

function runXSLProcessor(xml) {
    if (xsl == null)
        throw new Error("Could not load XSLT.");
    if (typeof XSLTProcessor == "undefined") {
        if (typeof xml.transformNode != "undefined") { // old IE
            return xml.transformNode(xsl);
        }
        if(window.ActiveXObject || "ActiveXObject" in window) { // IE 9+
            if(typeof xsl != "undefined") {
                xsltProcessor = xsl.createProcessor();
                xsltProcessor.input = xml;
                xsltProcessor.transform(xml);
                return xsltProcessor.output;
            }
        }
        throw new Error("This browser does not support processing XSLT.");
    }
    else {
        xsltProcessor = new XSLTProcessor();
        xsltProcessor.importStylesheet(xsl);
        return xsltProcessor.transformToDocument(xml);
    }
}

// set click handlers for links
function load_Document() {
    var loadXML = window.document.getElementById("loadXML");
    if (loadXML != null)
        loadXML.addEventListener("click", click_loadXML);
    var loadJSON = window.document.getElementById("loadJSON");
    if (loadJSON != null)
        loadJSON.addEventListener("click", click_loadJSON);

    loadXSL();
}

function loadXSL() {
    var req = getRequestObject();
    req.onreadystatechange = function () {
        if (req.readyState == 4 && req.status == 200 && (!req.responseType || req.responseType == "xml")) {
            if(window.ActiveXObject || "ActiveXObject" in window) { // IE 9+
				xsl = new ActiveXObject("Msxml2.FreeThreadedDOMDocument");
				if(typeof xsl != "undefined") {
                    xsl.loadXML(req.responseText);
                } else {
                    xsl = req.responseXML;
                }
            } else {
                xsl = req.responseXML;
            }
        }
    };
    req.open("GET", ROOT_URI + "VL2012_demo.xslt", true); // asynchronous request
    req.send("");
}

function replaceCourseData(newdatael) {
    var cd = window.document.getElementById("courseData");
    while (cd.hasChildNodes()) {
        cd.removeChild(cd.firstChild);
    }
    cd.appendChild(newdatael);
}

function click_loadXML(evt) {
    if (xsl == null)
        loadXSL();

    var req = getRequestObject();
    req.open("GET", ROOT_URI + "VL2012_demo.xml", false); // synchronous request
    req.send("");
    if (!req.responseType || req.responseType == "xml") {
        xml = null;
        if(window.ActiveXObject || "ActiveXObject" in window) { // IE 9+
            xml = new ActiveXObject("Msxml2.FreeThreadedDOMDocument");
            if(typeof xml != "undefined") {
                xml.loadXML(req.responseText);
            } else {
                xml = req.responseXML;
            }
        } else {
            xml = req.responseXML;
        }
        var trdoc = runXSLProcessor(xml);
        if (typeof trdoc == "string") {
            var tdoc = null;
            if (typeof window.DOMParser != "undefined") {
                parser = new window.DOMParser();
                trdoc = parser.parseFromString(trdoc, "application/xml");
            }
            else {
                try {
                    tdoc = new ActiveXObject("Microsoft.XMLDOM");
                    tdoc.loadXML(trdoc);
                    trdoc = tdoc;
                }
                catch (e) { throw new Error("Your browser does not support XMLDOM."); }
            }
        }
        var ci = trdoc.getElementById("courseinfo");
        if (ci) {
            replaceCourseData(window.document.importNode(ci, true));
        }
    }
    else
        throw new Error("Response was not an XML document! Response " + req.responseType + " " + req.responseText);

    evt.preventDefault();
    return false; // for older browsers
}

function click_loadJSON(evt) {
    var req = getRequestObject();
    req.open("GET", ROOT_URI + "VL2012_demo.json", false); // synchronous request
    req.send("");
    //req.responseXML;
    if (!req.responseType || req.responseType == "json") {
        var ci = eval("(" + req.responseText + ")"); // Firefox requires ()
        if (ci) {
            var cid = window.document.createElement("div");
            cid.setAttribute("id", "courseinfo");
            var cih = window.document.createElement("h2");
            cih.appendChild(window.document.createTextNode(ci.kood + " " + ci.nimi));
            cid.appendChild(cih);
            var cil = window.document.createElement("ol");
            cid.appendChild(cil);
            ci.teemad.forEach(function (it) {
                var cit = window.document.createElement("li");
                cil.appendChild(cit);
                cit.appendChild(window.document.createTextNode(it));
            });
            var cip = window.document.createElement("p");
            cid.appendChild(cip);
            cip.appendChild(window.document.createTextNode("Loengu toimumine: " + ci.loeng.paev + " " + ci.loeng.kell));
            cip.appendChild(window.document.createElement("br"));
            cip.appendChild(window.document.createTextNode("Ruum: " + ci.loeng.ruum));
            cip.appendChild(window.document.createElement("br"));
            cip.appendChild(window.document.createTextNode("Allikas: JSON"));
            replaceCourseData(cid);
        }
    }
    else
        throw new Error("Response was not an JSON document! Response " + req.responseType + " " + req.responseText);

    evt.preventDefault();
    return false; // for older browsers
}
