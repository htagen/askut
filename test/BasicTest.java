import junit.framework.TestCase;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class BasicTest extends TestHelper {
    
    
    private String username = "kasutaja1000";
    private String password = "lammas";
    private String email = "example@example.com";
    
    @Test
    public void loginLogoutTest(){
        login(username, password);
        WebElement mainHeadline = driver.findElement(By.id("mainRecentlyAskedAreaHeader"));
        assertEquals("HILJUTI KÜSITUD",mainHeadline.getText());
        
        logout();
        assertEquals("Vastused Sinu küsimustele. Anonüümselt.", driver.findElement(By.id("sloganID")).getText());
    }

    @Test
    public void loginFalsePassword() {
        login(username,"lamb");
        
        WebElement falsePswd = driver.findElement(By.className("errorText"));
        assertEquals("Vale parool!",falsePswd.getText());
    }

    @Test
    public void registerAccount(){
        String username = "UserForTesting";
        String password = "password";
        register(username, email, password, password);
    
        new WebDriverWait(driver,	waitForResposeTime).ignoring(
                StaleElementReferenceException.class).until(
                ExpectedConditions.visibilityOfElementLocated(By.className("noticeSuccess")));
        
        login(username,password);
    
        new WebDriverWait(driver,	waitForResposeTime).ignoring(
                StaleElementReferenceException.class).until(
                ExpectedConditions.visibilityOfElementLocated(By.id("profileBtn")));

        driver.findElement(By.id("profileBtn")).click();
    
        //delete the user
        driver.findElement(By.id("existingPassword")).sendKeys(password);
    
        new WebDriverWait(driver,	waitForResposeTime).ignoring(
                StaleElementReferenceException.class).until(
                ExpectedConditions.elementToBeClickable(By.id("deleteUser")));
    
        driver.findElement(By.id("deleteUser")).click();
        driver.findElement(By.id("loginBtn")).click();
        login(username,password);
        String errorText = driver.findElement(By.className("errorText")).getText();
    
        assertEquals("Sellist kasutajat ei eksisteeri.", errorText);
    }
    
    @Test
    public void registerPasswordNotMatchingTest(){
        register(username, email, "password", "wrong");
        List<WebElement> errors = driver.findElements(By.className("errorText"));
        ArrayList<String> arrayList = new ArrayList<String>();
    
        for (WebElement el : errors) {
            arrayList.add(el.getText());
        }
        assertTrue(arrayList.contains("Paroolid ei klapi omavahel"));
    }
    
    @Test
    public void registerExistingUserTest(){
        register(username, email, password, password);
        List<WebElement> errors = driver.findElements(By.className("errorText"));
        ArrayList<String> arrayList = new ArrayList<String>();
    
        for (WebElement el : errors) {
            arrayList.add(el.getText());
        }
        assertTrue(arrayList.contains("Kasutajanimi on juba olemas!"));
    }
    
    @Test
    public void changeUsersEmailFromProfile(){
        login(username,password);
        
        new WebDriverWait(driver,	waitForResposeTime).ignoring(
                StaleElementReferenceException.class).until(
                ExpectedConditions.visibilityOfElementLocated(By.id("profileBtn")));
    
        driver.findElement(By.id("profileBtn")).click();
    
        driver.findElement(By.id("email")).sendKeys((Keys.chord(Keys.CONTROL, "a")));
        driver.findElement(By.id("email")).sendKeys(Keys.ENTER);
    
        List<WebElement> errors = driver.findElements(By.className("errorText"));
        ArrayList<String> arrayList = new ArrayList<String>();
        for (WebElement el : errors) {
            arrayList.add(el.getText());
        }
        assertTrue(arrayList.contains("Parool on vajalik!"));
    
        driver.findElement(By.id("email")).sendKeys((Keys.chord(Keys.CONTROL, "a")));
        driver.findElement(By.id("email")).sendKeys("example2@example.com");
        driver.findElement(By.id("existingPassword")).sendKeys(password);
        driver.findElement(By.id("userInfoForm")).submit();
    }
    
    @Test
    public void loadMoreQuestions(){
        driver.findElement(By.id("anonymousEnter")).click();
        goToPage("KÜSIMUSED");
        List<WebElement> rows = driver.findElements(By.tagName("tr"));
        assert rows.size() == 3;
        
        driver.findElement(By.id("moreQuestions")).click();
        rows = driver.findElements(By.tagName("tr"));
        assertEquals(5, rows.size());
    }
    
    @Test
    public void sendFeedBackToAskUT(){
        driver.findElement(By.id("anonymousEnter")).click();
        goToPage("AskUT");
        driver.findElement(By.id("contactLink")).click();
        driver.findElement(By.id("contactEmailField")).sendKeys(email);
        driver.findElement(By.id("contactSubjectField")).sendKeys("subject");
        driver.findElement(By.id("contactBodyField")).sendKeys("This is body.");
        driver.findElement(By.className("sendBtn")).sendKeys(Keys.ENTER);
        
        assertEquals("Teade on saadetud!", driver.findElement(By.className("notice")).getText());
    }
    
    @Test
    public void changeLanguage(){
        assertEquals("Vastused Sinu küsimustele. Anonüümselt.", driver.findElement(By.id("sloganID")).getText());
        assertEquals("Logi sisse", driver.findElement(By.id("loginBtn")).getText());
        assertEquals("Logi sisse Facebookiga", driver.findElement(By.id("loginFbButton")).getText());
        assertEquals("Logi sisse ID-kaardiga", driver.findElement(By.id("loginIDButton")).getText());
        assertEquals("Registreeri", driver.findElement(By.id("registerButton")).getText());
        assertEquals("Sisene anonüümselt", driver.findElement(By.id("anonymousEnter")).getText());
    
        driver.findElement(By.className("eng")).click();
        
        assertEquals("Answers to Your questions. Anonymously.", driver.findElement(By.id("sloganID")).getText());
        assertEquals("Login", driver.findElement(By.id("loginBtn")).getText());
        assertEquals("Login with Facebook", driver.findElement(By.id("loginFbButton")).getText());
        assertEquals("Login with ID card", driver.findElement(By.id("loginIDButton")).getText());
        assertEquals("Sign up", driver.findElement(By.id("registerButton")).getText());
        assertEquals("Enter anonymously", driver.findElement(By.id("anonymousEnter")).getText());
    }
    
    @Test
    public void returnToSamePageAfterLogin(){
        String faqPage = "https://askut.today/home/faq.php";
        driver.get(faqPage);
        driver.findElement(By.id("loginBtnDropDown")).click();
        login(username,password);
    
        new WebDriverWait(driver,	waitForResposeTime).ignoring(
                StaleElementReferenceException.class).until(
                ExpectedConditions.visibilityOfElementLocated(By.id("kkk")));
        
        assertEquals(faqPage, driver.getCurrentUrl());
    }
}
