import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class TestHelper {
    
    static WebDriver driver;
    final int waitForResposeTime = 4;
    String baseUrl = "https://askut.today";
    String profileUrl = "https://askut.today/home/profile.php";
    String mainUrl = "https://askut.today/home/main.php";
    String logoutUrl = "https://askut.today/?logout=true";
    
    @Before
    public void setUp(){
        
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Hanna\\Documents\\17-18_Kevad\\Tarkvara_testimine\\lab5\\geckodriver-v0.20.1-win64\\geckodriver.exe");
        driver = new FirefoxDriver();
        
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseUrl);
    
        new WebDriverWait(driver, waitForResposeTime).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Got it!")));
        driver.findElement(By.linkText("Got it!")).click();
        
    }
    
    void goToPage(String page){
        WebElement elem = driver.findElement(By.linkText(page));
        elem.click();
    }
    
    void waitForElementById(String id){
        new WebDriverWait(driver, waitForResposeTime).until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
    }
    
    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        }
        catch (NoSuchElementException e) {
            return false;
        }
    }
    
    void login(String username, String password){
        driver.get(baseUrl);
        driver.findElement(By.id("loginBtn")).click();
        driver.findElement(By.id("loginUsernameField")).sendKeys(username);
        driver.findElement(By.id("loginPasswordField")).sendKeys(password);
    
        driver.findElement(By.id("loginPasswordField")).sendKeys(Keys.ENTER);
    }
    
    void logout(){
        driver.findElement(By.id("logOutBtn")).click();
    }
    
    void register(String username, String email, String password, String repeatPassword){
        driver.get(baseUrl);
        driver.findElement(By.id("registerButton")).click();
        
        driver.findElement(By.id("user")).sendKeys(username);
        driver.findElement(By.id("email")).sendKeys(email);
        driver.findElement(By.id("pass1")).sendKeys(password);
        driver.findElement(By.id("pass2")).sendKeys(repeatPassword);
    
        driver.findElement(By.id("pass2")).sendKeys(Keys.RETURN);
    
        new WebDriverWait(driver,	waitForResposeTime).ignoring(
                StaleElementReferenceException.class);
    }
    
    @After
    public void tearDown(){
        driver.close();
    }
    
}