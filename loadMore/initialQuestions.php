<?php
include '../database/connect.php';

$sql = "SELECT question, code, question.ID as \"qID\",question.name, question.lector, question.time as \"qTime\", count(answer) as \"answerNr\" 
        from question 
        left join answer 
        on question.ID=question_ID  
        GROUP BY question.ID 
        union 
        SELECT question, code, question.ID as \"qID\",question.name, question.lector, question.time as \"qTime\", count(answer) as \"answerNr\"  
        from question 
        right join answer 
        on question.ID=question_ID 
        GROUP BY question.ID 
        ORDER BY qTime desc";
$conn->set_charset('utf8');
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0){
    echo '<table class="tableAskut" id="questionTable"><tr ><th >KÜSIMUS</th><th >AINE KOOD</th><th >AINE NIMETUS</th><th >ÕPPEJÕUD</th><th >AEG</th><th >VASTUSEID</th></tr>';

    while ($row = mysqli_fetch_assoc($result)) {
        $qID = $row['qID'];
        if ($qID != 1) {
            echo '<tr id="'.$row['qID'].'" class="jsMainQuestionButton"><td >' . $row['question']. '</td><td >' . $row['code']. ' </td><td > ' . $row['name']. '</td><td > ' . $row['lector']. ' </td><td > ' . $row['qTime']. ' </td><td > ' . $row['answerNr']. ' </td></tr>';
        }
    }
    echo '</table>';
}