<?php

/**
 * Server-side file.
 * This file is an infinitive loop. Seriously.
 * It gets the server last-changed timestamp, checks if this is larger than the timestamp of the
 * AJAX-submitted timestamp (time of last ajax request), and if so, it sends back a JSON with the data from
 * data.txt (and a timestamp). If not, it waits for one seconds and then start the next while step.
 *
 * Note: This returns a JSON, containing the content of data.txt and the timestamp of the last data.txt change.
 * This timestamp is used by the client's JavaScript for the next request, so THIS server-side script here only
 * serves new content after the last file change. Sounds weird, but try it out, you'll get into it really fast!
 */

// set php runtime to unlimited
set_time_limit(0);

include_once '../../database/helper.php';
$servername = 'hostname';
$username = 'username';
$password = 'password';
$dbname = 'name';

//$qID = isset($_SESSION['qID']) ? (int)$_SESSION['qID'] : null;

// Andmebaasiga ühendamine
$conn = mysqli_connect(config($servername), config($username), config($password), config($dbname));

// Andmebaasi ühenduse kontroll
if ($conn->connect_error) {
    die('Connection failed: ' . $conn->connect_error);
}

$sql = 'select * from answer';
$conn->set_charset('utf8');
$queryResult = $conn->query($sql);

if ($queryResult === false){
    echo 'Midagi läks valesti. Proovi mõne aja pärast uuesti!';
}else {
// main loop
    while (true) {
        // if ajax request has send a timestamp, then $last_ajax_call = timestamp, else $last_ajax_call = null
        $last_ajax_call = isset($_GET['timestamp']) ? (int)$_GET['timestamp'] : null;

        // PHP caches file data, like requesting the size of a file, by default. clearstatcache() clears that cache
        clearstatcache();
        // get timestamp of when file has been changed the last time
        $last_change_in_data_file = filemtime($queryResult);

        // if no timestamp delivered via ajax or data.txt has been changed SINCE last ajax timestamp
        if ($last_ajax_call == null || $last_change_in_data_file > $last_ajax_call) {

            // get content of data.txt
            $data = file_get_contents($queryResult);

            // put data.txt's content and timestamp of last data.txt change into array
//            while($row = $result->fetch_assoc()) {
//                echo '<tr ><td >' . $row['question']. '</td><td >' . $row['lector']. ' </td><td > ' . $row['name']. '</td><td > ' . $row['institute']. ' </td><td > ' . $row['time']. ' </td></tr>';
//            }

            $resultFromDB = '';
            while($row = $queryResult->fetch_assoc()) {
                $resultFromDB .= '<span>'.$row['username'].' - </span><span>' .$row['answer']. '  ('.$row['time'].')</span><br><br>';
            }

            $result = array(
                'dataFromDB' => $resultFromDB,
                'timestamp' => $last_change_in_data_file
            );

            // encode to JSON, render the result (for AJAX)
            $json = json_encode($result);
            echo $json;

            break;

        } else {
            sleep(5);
            continue;
        }
    }
}
