<?php
/*
 * Kasutatud seda allikat ja juhendit: https://bitbucket.org/smartid/loginphp/src/c4f7ba4691c01f601494626b0758ee16a5b3cb41?at=master
 * */

include_once 'helperID.php';

if (isset($_GET["error"])) {
    echo("<pre>OAuth Error: " . $_GET["error"] . "\n");
    echo('<a href="idLogin.php">Retry</a></pre>');
    die;
}

$clientId = configID('clientId');
$clientSecret = configID('clientSecret');
$redirectUrl = "https://askut.today/home/main.php";

$authorizeUrl = 'https://id.smartid.ee/oauth/authorize';
$accessTokenUrl = 'https://id.smartid.ee/oauth/access_token';


require("client.php");
require("grantType/iGrantType.php");
require("grantType/authorizationCode.php");

$client = new OAuth2\Client($clientId, $clientSecret, OAuth2\Client::AUTH_TYPE_AUTHORIZATION_BASIC);
$client->setCurlOption(CURLOPT_USERAGENT, "UserAgent");

if (!isset($_GET["code"]) && isset($_GET["idLogin"])  ) {
    $authUrl = $client->getAuthenticationUrl($authorizeUrl, $redirectUrl, array());
    header("Location: " . $authUrl);
    die("Redirect");

} elseif (isset($_GET["code"])) {
    $params = array("code" => $_GET["code"], "redirect_uri" => $redirectUrl);
    $response = $client->getAccessToken($accessTokenUrl, "authorization_code", $params);

    $accessTokenResult = $response["result"];
    $client->setAccessToken($accessTokenResult["access_token"]);
    $client->setAccessTokenType(OAuth2\Client::ACCESS_TOKEN_BEARER);

    $response = $client->fetch("https://id.smartid.ee/api/v2/user_data");
    $_SESSION['idUserId'] = $response['result']['idcode'];
    $_SESSION['idUserEmail'] = $response['result']['email'];
    include_once '../database/server.php';

}
